<?php session_start();
include "header.php";
$instance = new dbfunctions;

$page="students";
include "sidebar.php";
//list of schools
$schools=$db->prepare("select * from schools");
$schools->execute();
$rows = $schools->fetchAll();
$id =$_REQUEST['id'];

	$comp=$db->prepare("select * from students where st_id = :id");
	$comp->bindParam(':id',$id);
	$comp->execute();
	$compdet = $comp -> fetch();

if($_REQUEST['submit']!="")
{
	 $res=$instance->updation($_REQUEST); 
	 if($res==""){
	 echo "<script  language='javascript'>window.location='students_list.php';</script>";
	 }	
}
?>


<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='students_list.php' title='Students List' class='tip-bottom'>".$page;?></a><a href='#' title='' style="cursor:none" class='tip-bottom'>edit</a></div>
  
  </div>
<!--End-breadcrumbs-->


<!--Registration of students-->
  <div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Update Student</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%">
          <form action="edit.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Student Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Student Name" name="stname" id="stname" value="<?php echo $compdet['st_name'];?>"  required/>
              </div>
            </div>
 	    <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Gender :</label>
              <div class="controls">
                  <input type="radio" <?php if($compdet['gender'] == "M") echo "checked==checked";?> name="gender" value="M" required> Male <input type="radio" name="gender" <?php if($compdet['gender'] == "F") echo "checked==checked";?> value="F" required> Female
              </div>
            </div>
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Class :</label>
              <div class="controls">
                <select id="class" name="class" required>
                <option value="">Select</option>
		<option value="Pre-KG"  <?php if ($compdet["class"] =="Pre-KG") echo "selected=selected";?>>Pre-KG</option>
		<option value="LKG"  <?php if ($compdet["class"] =="LKG") echo "selected=selected";?>>LKG</option>
		<option value="UKG"  <?php if ($compdet["class"] =="UKG") echo "selected=selected";?>>UKG</option>
		    <?php
		        for($i=1;$i<=12;$i++){?>
		      <option value="<?php echo $i;?>" <?php if($compdet["class"] == $i) echo "selected==selected";?>> <?php echo $i;?></option>
		<?php } ?>
               </select>
              </div>
            </div>
           <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Section :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Section" name="section" id="section" value="<?php echo $compdet['section'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  School  :</label>
              <div class="controls">
		<select id="school" name="school" required>
                <option value="">Select</option>
		    <?php
		        for($i=0;$i<count($rows);$i++){?>
		      <option value="<?php echo $rows[$i]['id_schools'];?>" <?php if($compdet["id_schools"] == $rows[$i]['id_schools']) echo "selected==selected";?>><?php echo $rows[$i]['school_name'];?></option>
			<?php }?>
               </select>
              </div>
            </div>
  	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Student ID :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Parent Name" name="studentid" id="studentid" value="<?php echo $compdet['student_id'];?>"  readonly/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Parent Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Parent Name" name="pname" id="pname" value="<?php echo $compdet['parent_name'];?>"  required/>
              </div>
            </div>
                      
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Phone Number :</label>
              <div class="controls">
                <input type="number" class="" placeholder="Phone Number" name="phone" id="phone" value="<?php echo $compdet['phone'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Address :</label>
              <div class="controls">
		<textarea class="" placeholder="Address" name="address" id="address"><?php echo $compdet['address'];?></textarea>
              </div>
            </div>
	  	                  <input type="hidden" name="id" value=<?php echo $id;?>>		
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Update" class="btn btn-success" />	<input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
<!--Registration of students-->

  
</div>
</div>
<script>
function closed()
{
	window.location.href="students_list.php";
}
</script>
<?php
include "footer.php";

?>

 
