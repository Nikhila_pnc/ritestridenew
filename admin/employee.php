<?php session_start();
include "header.php";
$instance = new dbfunctions;

$page="Employee";
include "sidebar.php";


if($_REQUEST['submit']!="")
{
	 $res=$instance->registration($_REQUEST); 
	 if($res==""){
	 echo "<script  language='javascript'>window.location='index.php';</script>";
	 }	
}
?>


<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->


<!--Action boxes-->
   <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Register Employees</h5>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:50%;background-color:#fff;color:#000;margin-left:10%">
          <form action="employee.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
            <div class="control-group">
              <label class="control-label">Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Name" name="name" id="name" value="<?php echo $_REQUEST['name'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Email Address :</label>
              <div class="controls">
                <input type="email" class="" placeholder="Email"  name="email" id="email" value="<?php echo $_REQUEST['email'];?>" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Password :</label>
              <div class="controls">
                <input type="password"  class="" placeholder="Enter Password" name="password" id="password" required />
              </div>
            </div>
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Save" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
<!--End-Action boxes-->    
</div>
</div>
<?php
include "footer.php";

?>

 
