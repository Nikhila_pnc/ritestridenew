<?php session_start();
include "header.php";

$page="events";
include "sidebar.php";

$id =$_REQUEST['id'];

	$comp=$db->prepare("select * from schools where id_events = :id");
	$comp->bindParam(':id',$id);
	$comp->execute();
	$compdet = $comp -> fetch();
	$name= $compdet['event_name'];
	$details=$compdet['event_details'];
	$date=$compdet['event_date'];
?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='events_list.php' title='Event List' class='tip-bottom'>".$page;?></a><a href='#' title='' style="cursor:none" class='tip-bottom'>edit</a></div>
  </div>
<?php
if($_REQUEST['submit']=="Update")
{
	$name = $_REQUEST['event'];	
	$details= $_REQUEST['details'];
	$date=$_REQUEST['date'];
	$id = $_REQUEST['id'];	

	try{
		$stmt = $db->prepare("UPDATE events SET event_name =?, event_details=?, event_date=? WHERE id_events=?");
		$stmt->execute(array($name,$details,$date,$id));	
	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='events_list.php';</script>";
}
?>


<br><br>
<div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Event Details</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:50%;background-color:#fff;color:#000;margin-left:5%">
          <form action="event_edit.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
           
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Event Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Event Name" name="event" id="event" value="<?php echo $name;?>"  required/>
              </div>

	   </div>
	   <div class="control-group">
              <label class="control-label">  Event Details :</label>
	   <div class="controls">
		<textarea name="details" id="details" placeholder="School Details"><?php echo $details;?></textarea>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Event Date :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Event Date" name="date" id="date" value="<?php echo $date;?>"  required/>
              </div>

	   </div>
	                  <input type="hidden" name="id" value=<?php echo $id;?>>		
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Update" class="btn btn-success" /> <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>

</div>
<script>
function closed()
{
	window.location.href="events_list.php";
}
</script>
<?php
include "footer.php";
?>

 
