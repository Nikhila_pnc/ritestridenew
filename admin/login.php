<?php session_start();
include("includes/common.php");

//echo "Hi".$_REQUEST['submit'];
if($_REQUEST['submit']=="Login")
{
		$stmt=$db->prepare("select * from admin where username = :username and password=:password");
	        $stmt->bindParam(':username', $_REQUEST['username']);
	        $stmt->bindParam(':password', $_REQUEST['password']);
	        
	        $stmt->execute();
	        $details = $stmt -> fetch();       
	        $count = $stmt->rowCount();
	        if ($count>0)
	        {   
	      
	       
	        //$val1=strcmp($_REQUEST['username'],"admin@jivass");
	       // $val2=strcmp($_REQUEST['password'],"pwd@admin");
	       // if (($val1==0) && ($val2==0))
	        //{             
	             $_SESSION['admin']= "Rite Stride";
		     $_SESSION['admin_id']= $details['admin_id'];
	              $_SESSION['start_time_admin'] = time();
	                $date=date('Y-m-d H:i:s');        
	            echo "<script  language='javascript'>window.location='index.php';</script>";
			}
			else
		           $res="Please enter correct username and password";
}
?>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Rite Stride Admin</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="css/matrix-login.css" />
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>

	 <!--center><h2 style="color:#ffffff"> Rite Stride Pvt Ltd</h2></center-->
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" method="POST" action="login.php">
				 <div class="control-group normal_text"> <a href="http://ritestride.in/" target="_blank"><h3><img src="img/logo.png" alt="Logo" /></a></h3></div>
		 <?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-envelope"> </i></span><input type="text" placeholder="Username" name="username" required/>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" placeholder="Password" name="password" required/>
                        </div>
                    </div>
                </div>
              
                <div class="form-actions">
                    <center><input type="submit" name="submit" value="Login" class="btn btn-success" /></center>
		    <br><p align="center" ><a href="forgotpwd.php" style="cursor:pointer;color:#fff">Forgot Password?</a></p>
                </div>
            </form>
        </div>
        
        <script src="js/jquery.min.js"></script>  
        <script src="js/matrix.login.js"></script> 
    </body>

</html>


