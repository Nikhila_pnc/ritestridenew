<?php session_start();
/*
	author 		:   Anjali
	File Name	:   functions.class.php
	Purpose		:   For admin 
*/
class dbfunctions
{
	function dbfunctions()
	{
			$this->db = new PDO('mysql:host=localhost;dbname=webapp_ritestride;charset=utf8mb4', 'phpmyadmin', 'Jiva@123');
	}

	
	function registration($arr)
	{
		$stname= $arr['stname'];
		$gender= $arr['gender'];
		$class= $arr['class'];
		$section= $arr['section'];
		$school= $arr['school'];
		$pname= $arr['pname'];		
		$email=$arr['email'];
		$pwd=$arr['password'];
		$phone= $arr['phone'];
		$address= $arr['address'];

		try{
			$stmt1= $this->db->prepare("select * from students where email = :pemail");
       			$stmt1->bindParam(':pemail', $email);
        		$stmt1->execute();
        		$details = $stmt1 -> fetch();       
		        $count = $stmt1->rowCount();
        		if($count>0)
			{ return "Email id already exists";}
			else{
				//echo "Hi";
				$stmt = $this->db->prepare("INSERT INTO students(st_name,gender,class,section,id_schools,parent_name,email,password,phone,address) VALUES (:st_name,:gender,:class,:section,:school_details,:parent_name,:email,:password,:phone,:address)");
				$stmt->bindParam(':st_name', $stname);
				$stmt->bindParam(':gender', $gender);
				$stmt->bindParam(':class', $class);
				$stmt->bindParam(':section', $section);
				$stmt->bindParam(':school_details', $school);
				$stmt->bindParam(':parent_name', $pname);
				$stmt->bindParam(':email', $email);
				$stmt->bindParam(':password', $pwd);
				$stmt->bindParam(':phone', $phone);
				$stmt->bindParam(':address', $address);
				$stmt->execute();
				
				$id = $this->db->lastInsertId();
				$stid= "RS".rand(100000,1000000);
				
				$check = $this->check_studentid($stid);
                                if($check == ""){

				$stmt1 = $this->db->prepare("UPDATE students SET student_id =? WHERE st_id=?");
				$stmt1->execute(array($stid,$id));
				
				//if (!$stmt->execute()) {
					//print_r($stmt->errorInfo());
				//}


			$globalemail= "info@ritestride.in";
				//for registering person
			 $to = $email;
			 $subject = "Registration with Rite Stride";
			 $message = "Dear ".ucfirst(strtolower($pname))."<br><br> Thank you for connecting with us. Your kid's Id for login is : ".$stid." and your password is :".$pwd."<br><br> Thanks. </br>RiteStride Team.";
				
			 $headers = "From:".$globalemail . "\r\n";
			 $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			 mail($to,$subject,$message,$headers);
			 }
			}
		}
		catch(PDOException $e)
			{
				echo "Connection failed: " . $e->getMessage();
			}
	}
      function check_studentid($studentid)
	{
		$stmt1= $this->db->prepare("select * from students where student_id = :student_id");
       		$stmt1->bindParam(':student_id', $studentid);
        	$stmt1->execute();
        	$details = $stmt1 -> fetch();       
		$count = $stmt1->rowCount();
		return $count;
	}
      function fitness($arr)
	{
		$term = $arr['term'];
		$year = $arr['year'];	
		$studentid= $arr['studentid'];
		$height = $arr['height'];
		$height_measure = $arr['heightin'];
		$weight = $arr['weight'];
		$weight_measure = $arr['weightin'];
		$bmi = $arr['bmi'];
		$endurance = $arr['endurance'];
		$upper_body = $arr['es_upperbody'];
		$lower_body = $arr['es_lowerbody'];
		$flexibility = $arr['flexibility'];
		$stest =$arr['stest'];
		$overallgrade= $arr['overall'];


		try{
			$stmt1= $this->db->prepare("select * from fitness_assessment where term = :terms and st_id = :stid");
       			$stmt1->bindParam(':terms', $term);
       			$stmt1->bindParam(':stid', $studentid);
        		$stmt1->execute();
        		$details = $stmt1 -> fetch();       
		        $count = $stmt1->rowCount();
        		if($count>0)
			{ return  $term." term values are already entered";}
			else{
			$stmt = $this->db->prepare("INSERT INTO fitness_assessment(academic_year,term,st_id,height,height_measure,weight,weight_measure,BMI,endurance,upper_body,lower_body,flexibility,stest,grade) VALUES (:year,:term,:st_id,:height,:height_measure,:weight,:weight_measure,:bmi,:endurance,:upper_body,:lower_body,:flexibility,:stest,:grade)");
				$stmt->bindParam(':year', $year);
				$stmt->bindParam(':term', $term);
				$stmt->bindParam(':st_id', $studentid);
				$stmt->bindParam(':height', $height);
				$stmt->bindParam(':height_measure', $height_measure);
				$stmt->bindParam(':weight', $weight);
				$stmt->bindParam(':weight_measure', $weight_measure);
				$stmt->bindParam(':bmi', $bmi);
				$stmt->bindParam(':endurance', $endurance);
				$stmt->bindParam(':upper_body', $upper_body);
				$stmt->bindParam(':lower_body', $lower_body);
				$stmt->bindParam(':flexibility', $flexibility);
				$stmt->bindParam(':stest', $stest);
				$stmt->bindParam(':grade', $overallgrade);
				$stmt->execute();
			//if (!$stmt->execute()) {
					//print_r($stmt->errorInfo());
				//}
			}
		   }
			catch(PDOException $e)
			{
				echo "Connection failed: " . $e->getMessage();
			}
		
	}

       function skills($arr)
	{
		$year = $arr['year'];	
		$studentid= $arr['studentid'];
		$overallgrade= $arr['overall'];

		$stmt1= $this->db->prepare("select * from assess_term where accademic_year = :year and st_id = :stid");
       			$stmt1->bindParam(':year', $year);
       			$stmt1->bindParam(':stid', $studentid);
        		$stmt1->execute();
        		$details = $stmt1 -> fetch();       
		        $count = $stmt1->rowCount();
        		if($count>0)
			{ return "This accademic year values are already entered";}
			else{
				$stmt = $this->db->prepare("INSERT INTO assess_term(accademic_year,st_id,overall_grade) VALUES (:year,:stid,:overallgrade)");
				$stmt->bindParam(':year', $year);
				$stmt->bindParam(':stid', $studentid);
				$stmt->bindParam(':overallgrade', $overallgrade);
				$stmt->execute();
				$termid = $this->db->lastInsertId();

			$class = $arr['class'];
			
			$assign=$this->db->prepare("select * from unit_assign");
			$assign->execute();
			$assignrows = $assign->fetchAll();	


			for($i=0;$i<count($assignrows);$i++)
			{
				$classes=explode(",",$assignrows[$i]['classes']);
				if(in_array($class, $classes)){
				 $id_units= $assignrows[$i]['id_units'];

			  //list of units
				$ut1=$this->db->prepare("select * from units where id_units =:sid");
				$ut1->bindParam(':sid', $id_units);
				$ut1->execute();
				$utrows1 = $ut1->fetch();

				$ut2=$this->db->prepare("select * from unit_questions where id_units =:sid");
				$ut2->bindParam(':sid', $id_units);
				$ut2->execute();
				$utrows2 = $ut2->fetchAll();
				for($j=0;$j<count($utrows2);$j++){
					$quesid  = $utrows2[$j]['id_unit_ques'];
					$score= $arr["Score".$quesid];


					$stmt = $this->db->prepare("INSERT INTO unit_assess(id_unit_ques,score,id_assess_term) VALUES (:quesid,:score,:termid)");
					$stmt->bindParam(':quesid', $quesid);
					$stmt->bindParam(':score', $score);
					$stmt->bindParam(':termid', $termid);
					$stmt->execute();
				}
				}
			}
		}
	}
       	function updation($arr)
	{
		$stname= $arr['stname'];
		$studentid= $arr['studentid'];
		$stid= $arr['id'];
		$gender= $arr['gender'];
		$class= $arr['class'];
		$section= $arr['section'];
		$school= $arr['school'];
		$pname= $arr['pname'];		
		$phone= $arr['phone'];
		$address= $arr['address'];

		try{
				//echo "Hi";UPDATE schools SET school_name =?, school_details=? WHERE id_schools=?
				$stmt = $this->db->prepare("UPDATE students SET st_name=?, gender=?, class=?, section=?, id_schools=?, student_id=?, parent_name=?, phone=?, address=? where st_id=?");
				$stmt->execute(array($stname,$gender,$class,$section,$school,$studentid,$pname,$phone,$address,$stid));
if (!$stmt->execute()) {
					print_r($stmt->errorInfo());
				}
		}
		catch(PDOException $e)
			{
				echo "Connection failed: " . $e->getMessage();
			}
	}

}	
?>
