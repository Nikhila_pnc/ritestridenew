<?php
 
class Paginator{
    var $items_per_page;
    var $items_total;
    var $current_page;
    var $num_pages;
    var $mid_range;
    var $low;
    var $high;
    var $limit;
    var $return;
    var $default_ipp =50;
 
    function Paginator()
    {
        $this->current_page = 1;
        $this->mid_range =5;
        $this->items_per_page = (!empty($_GET['ipp'])) ? $_GET['ipp']:$this->default_ipp;
    }
 
    function paginate()
    {
        if($_GET['ipp'] == 'All')
        {
            $this->num_pages = ceil($this->items_total/$this->default_ipp);
            $this->items_per_page = $this->default_ipp;
        }
        else
        {
            if(!is_numeric($this->items_per_page) OR $this->items_per_page <= 0) $this->items_per_page = $this->default_ipp;
            $this->num_pages = ceil($this->items_total/$this->items_per_page);
        }
        $this->current_page = (int) $_GET['page']; // must be numeric > 0
        if($this->current_page < 1 Or !is_numeric($this->current_page)) $this->current_page = 1;
        if($this->current_page > $this->num_pages) $this->current_page = $this->num_pages;
        $prev_page = $this->current_page-1;
        $next_page = $this->current_page+1;
 
        $link = "";
        $linkstring = explode("?","http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]);
        if(count($linkstring)==1)  $link = $linkstring[0]."?"; 
        else{
        		
        	 	if(strpos($linkstring[1],'page')===false)
        	 	{  
        	 		$link = $linkstring[0]."?".$linkstring[1]."&";
        	 	}
        	 	else 
        	 	{
        	 		$ques=explode("page", $linkstring[1]);
        	 		$link = $linkstring[0]."?".$ques[0];
        	 	}
        	 	
      	    }
      	    $link = str_replace(" ", "", $link);
      	    
        if($this->num_pages > 1)
        {        	       	
            $this->return = ($this->current_page != 1 And $this->items_total >= 10) ? "<a class=\"paginate\" href=\"".trim($link)."page=".$prev_page."&ipp=".$this->items_per_page."\">Prev</a> ":"<span class=\"inactive\" href=\"#\">Prev</span> ";
 
            $this->start_range = $this->current_page - floor($this->mid_range/2);
            $this->end_range = $this->current_page + floor($this->mid_range/2);
 
            if($this->start_range <= 0)
            {
                $this->end_range += abs($this->start_range)+1;
                $this->start_range = 1;
            }
            if($this->end_range > $this->num_pages)
            {
                $this->start_range -= $this->end_range-$this->num_pages;
                $this->end_range = $this->num_pages;
            }
            $this->range = range($this->start_range,$this->end_range);
 
            for($i=1;$i<=$this->num_pages;$i++)
            {
                if($this->range[0] > 2 And $i == $this->range[0]) $this->return .= " ... ";
                // loop through all pages. if first, last, or in range, display
                if($i==1 Or $i==$this->num_pages Or in_array($i,$this->range))
                {
                    $this->return .= ($i == $this->current_page And $_GET['page'] != 'All') ? "<a title=\"Go to page $i of $this->num_pages\" class=\"current\" href=\"#\">$i</a> ":"<a class=\"paginate\" title=\"Go to page $i of $this->num_pages\" href='".$link."page=".$i."&ipp=".$this->items_per_page."'>$i</a> ";
                }
                if($this->range[$this->mid_range-1] < $this->num_pages-1 And $i == $this->range[$this->mid_range-1]) $this->return .= " ... ";
            }
          $this->return .= (($this->current_page != $this->num_pages And $this->items_total >= 10) And ($_GET['page'] != 'All')) ? "<a class=\"paginate\" href='".$link."page=".$next_page."&ipp=".$this->items_per_page."'>Next</a>\n":"<span class=\"inactive\" href=\"#\">Next</span>\n";
          //  $this->return .= ($_GET['page'] == 'All') ? "<a class=\"current\" style=\"margin-left:10px\" href=\"#\">All</a> \n":"<a class=\"paginate\" style=\"margin-left:10px\" href=\"$link page=1&ipp=All\">All</a> \n";
        }
        else
        {
            for($i=1;$i<=$this->num_pages;$i++)
            {
                $this->return .= ($i == $this->current_page) ? "<a class=\"current\" href=\"#\">$i</a> ":"<a class=\"paginate\" href=\"".$link."page=".$i."&ipp=".$this->items_per_page."\">".$i."</a> ";
            }
             //if($this->num_pages > 1)  $this->return .= "<a class=\"paginate\" href=\"$link page=1&ipp=All\">All</a> \n";
        }
        $this->low = ($this->current_page-1) * $this->items_per_page;
        $this->high = ($_GET['ipp'] == 'All') ? $this->items_total:($this->current_page * $this->items_per_page)-1;
        $this->limit = ($_GET['ipp'] == 'All') ? "":" LIMIT $this->low,$this->items_per_page";
    }
 
    function display_items_per_page()
    {
        $link = "";
        $linkstring = explode("?", "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]);
        if(count($linkstring)==1)  $link = $linkstring[0]."?";
        else{ 
        	 	if(strpos($linkstring[1],'page')===false)
        	 	{ $link = $linkstring[0]."?".$linkstring[1]."&";}
        	 	else 
        	 	{
        	 		$ques=explode("page", $linkstring[1]);
        	 		$link = $linkstring[0]."?".$ques[0];
        	 	}
      	    }
      	    $link = str_replace(" ", "", $link);
      	    
        $items = '';
        $ipp_array = array(10,25,50);
        foreach($ipp_array as $ipp_opt)    $items .= ($ipp_opt == $this->items_per_page) ? "<option selected value=\"$ipp_opt\">$ipp_opt</option>\n":"<option value=\"$ipp_opt\">$ipp_opt</option>\n";
        return "<span class=\"paginate\">&nbsp; &nbsp;items per page : </span><select id='selectid' class=\"paginate\" onchange=\"window.location='".$link."page=1&ipp='+this[this.selectedIndex].value;return false\">$items</select>\n";
    }
 
    function display_jump_menu()
    {
     $link = "";
        $linkstring = explode("?", "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]);
        if(count($linkstring)==1)  $link = $linkstring[0]."?";
        else{ 
        	 	if(strpos($linkstring[1],'page')===false)
        	 	{ $link = $linkstring[0]."?".$linkstring[1]."&";}
        	 	else 
        	 	{
        	 		$ques=explode("page", $linkstring[1]);
        	 		$link = $linkstring[0]."?".$ques[0];
        	 	}
      	    }
      	    $link = str_replace(" ", "", $link);
      	    
        for($i=1;$i<=$this->num_pages;$i++)
        {
            $option .= ($i==$this->current_page) ? "<option value=\"$i\" selected>$i</option>\n":"<option value=\"$i\">$i</option>\n";
        }
        return "<span class=\"paginate\">&nbsp; &nbsp;page : </span><select id='selectid' class=\"paginate\" onchange=\"window.location='".$link."page='+this[this.selectedIndex].value+'&ipp=$this->items_per_page';return false\">$option</select>\n";
    }
 
    function display_pages()
    {
        return $this->return;
    }      
}
?>
