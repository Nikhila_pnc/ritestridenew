<?php session_start();
include "header.php";
include_once ('./classes/paginator.php');

$page="units";
include "sidebar.php";


$sql= "select  * from units" ;
$stmts=$db->prepare($sql);
$stmts->execute();
$num_rows = $stmts->rowCount();

if($num_rows>0){
	$pages = new Paginator;
	$pages->items_total = $num_rows;
	$pages->mid_range = 3; // Number of pages to display. Must be odd and > 3
	$pages->paginate();

	$sql=$sql." ".$pages->limit;
	$listing=$db->prepare($sql);
	$listing->execute();
	$rowct=$listing->rowCount();
	$result = $listing->fetchAll(PDO::FETCH_ASSOC);
}

?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='unit_list.php' title='Go to Home' class='tip-bottom'>".$page;?></a></div>
  
  </div>
<!--End-breadcrumbs-->


<!--Action boxes-->
  <div class="container-fluid"><br><br>
   <form method="post" action="school_list.php">
   <h4 align="left">Units</h4>
   <?php if($num_rows>0){?>
	<div align="right"><a href='unit_add.php'><input type='button' value='Add Units'></a></div>
         <table cellpadding="0" cellspacing="0" align="center" style=" border: 1px solid #ccc;color:#000" width="100%" class="table">
	     <tr  style="background-color: #3973ac;color:#FFF"><td colspan="6" align="right"> <?php  echo "<span class=\"paginate\">Page: $pages->current_page of $pages->num_pages</span>";	 
			echo " &nbsp; &raquo &nbsp; ". $pages->display_pages();echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";?></td></tr> 
        
   	     <tr>
                <td align="center">&nbsp;</td>
                <td align="center"><b>Units</b></td>
                <td align="left"><b>Objectives</b></td>
                <td align="left"><b>Classes</b></td>
		<td align="left"><b>Questions</b></td>
                <td align="center"><b>Edit</b></td>
          </tr>

<?php
$count =0;
$cr=1;
if($rowct>0){
foreach($result as $val) 
{
    if($cr%2==0)
    {$color="#CCC";}else{$color="#ECECE1";}
	   ?>
	   
          <tr bgcolor="<?php echo $color;?>" style="height:30px;">
		  <td align="center">&nbsp;</td>
          <td align="center"><?php echo $val['unit_name'];  ?></td>
          <td align="left"><?php echo $val['objective'];  ?></td>
          <td align="left"> <?php       $comp=$db->prepare("select * from unit_assign where id_units = :id");
					$comp->bindParam(':id',$val['id_units']);
					$comp->execute();
					$compdet = $comp -> fetch();
					echo $compdet['classes'];
			  ?>

		<br><a href="assign.php?id=<?php echo $val['id_units'];  ?>"><input type="button" value="+"></a></td>
          <td align="left"><a href="questions.php?id=<?php echo $val['id_units'];  ?>"><input type="button" value="View"></a></td>
          <td align="center"><a href="unit_edit.php?id=<?php echo $val['id_units'];  ?>"><img src="includes/icons/icon_edit.gif" width="20" height="20" border="0"></a></td>
          </tr>
                 <?php
	   $cr++;
	   $count++;
	   
	   }
   ?>		    
         <tr  style="background-color:#3973ac;color:#FFF"><td colspan="6" align="right"> <?php  echo "<span class=\"paginate\">Page: $pages->current_page of $pages->num_pages</span>";	 
			echo " &nbsp; &raquo &nbsp; ". $pages->display_pages();echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";?></td></tr> 
         </table></form> <?php  }}else echo "<h3 style='align:center'>No Content in the Database <a href='unit_add.php'><input type='button' value='Add Units'></a></h3>";?>
		
<!--End-Action boxes-->    
</div>
</div>
<?php
include "footer.php";
?>

 
