<?php session_start();
include "header.php";

$page="events";
include "sidebar.php";
?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='events_list.php' title='Events List' class='tip-bottom'>".$page;?></a><a href='#' title='' style="cursor:none" class='tip-bottom'>add</a></div>
  </div>
<?php
if($_REQUEST['submit']=="Submit")
{
	$eventname = $_REQUEST['event'];	
	$details= $_REQUEST['details'];
	$date=$_REQUEST['date'];

	
	try{
		$stmt = $db->prepare("INSERT INTO events(event_name,event_details,event_date) VALUES (:name,:det,:dat)");
		$stmt->bindParam(':name', $eventname);
		$stmt->bindParam(':det', $details);
		$stmt->bindParam('dat',$date);
		$stmt->execute();
	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='events_list.php';</script>";
}
?>


<br><br>
<div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Event Details</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:50%;background-color:#fff;color:#000;margin-left:5%">
          <form action="events.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
           
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Event Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Event Name" name="event" id="event" value="<?php echo $_REQUEST['event'];?>"  required/>
              </div>

	   </div>
	   <div class="control-group">
              <label class="control-label">  Event Details :</label>
	   <div class="controls">
		<textarea name="details" id="details" placeholder="Event Details"><?php echo $_REQUEST['details'];?></textarea>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Event Date :<br>(YYYY:MM:DD)</label>
              <div class="controls">
                <input type="text" class="" placeholder="Event Date" name="date" id="date" value="<?php echo $_REQUEST['date'];?>"  required/>
              </div>

	   </div>

	  
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Submit" class="btn btn-success" /> <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>

</div>
<script>
function closed()
{
	window.location.href="events_list.php";
}
</script>
<?php
include "footer.php";
?>

 
