<?php session_start();
include "header.php";

$page="change_pwd";
include "sidebar.php";

?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='change_pwd.php' title='Change Pwd' class='tip-bottom'>".$page;?></a></div>
  </div>
<?php
if($_REQUEST['submit']=="Update")
{
	$id= $_SESSION['admin_id'];
	$pwd = $_REQUEST['password'];	
	

	try{
		$stmt = $db->prepare("UPDATE admin SET password =? WHERE admin_id=?");
		$stmt->execute(array($pwd,$id));	
			
	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='index.php';</script>";
}
?>


<br><br>
<div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Change Password</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:50%;background-color:#fff;color:#000;margin-left:5%">
          <form action="change_pwd.php" method="post" class="form-horizontal" enctype="multipart/form-data">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
           
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  New Password :</label>
              <div class="controls">
                <input type="password" class="" placeholder="New Password" name="password" id="password"   required/>
              </div>

	   </div>

	  <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Update" class="btn btn-success" />  <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
</div>
</div>
<script>
function closed()
{
	window.location.href="index.php";
}
</script>
<?php
include "footer.php";
?>

 
