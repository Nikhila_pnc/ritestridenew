<?php session_start();
include "header.php";

$page="";
include "sidebar.php";


?>

<!--main-container-part-->

<!--Action boxes-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo ">".$page;?></div>
  </div>



<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
     <h3 style="text-align:left">Tracking Details</h3>
      <ul class="quick-actions">
        <li class="bg_lo"  style="border-radius:25px"> <a href="assessment.php"> <i class="icon-th-list"></i> ASSESSMENT</a> </li>
  	
	<li class="bg_lv"  style="border-radius:25px"> <a href="nutrition.php">   <i class="icon-leaf"></i> NUTRITION </a> </li>
  <li class="bg_ly"  style="border-radius:25px"> <a href="event.php">   <i class="icon-calendar"></i> EVENTS </a> </li>
  <li class="bg_dg"  style="border-radius:25px"> <a href="gallery.php">   <i class="icon-picture"></i> GALLERY </a> </li>


      </ul>
     </div>


<!--End-Action boxes-->    
	</div>
</div>
<?php
include "footer.php";
?>

 
