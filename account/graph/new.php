<?php 
$year =$_REQUEST['year'];
$sid = $_REQUEST['id'];;
$about=$_REQUEST['param'];

//$year = '2018-2019';
//$sid = 8;
//$about="height";

?>
<!DOCTYPE html>
<html>
  <head>
    <title>LineGraph :- <?php echo ucfirst($about);?> </title>
    <style>
      .chart-container {
        width: 1280px;
        height: 720px;
      }
    </style>
  </head>
  <body>
    <div class="">
	<input type="hidden" name="year" id="year" value="<?php echo $year;?>">
	<input type="hidden" name="id" id="id" value="<?php echo $sid;?>">
	<input type="hidden" name="param" id="param" value="<?php echo $about;?>">
      <canvas id="mycanvas"></canvas>
    </div>
    
    <!-- javascript -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/Chart.min.js"></script>
    <script type="text/javascript" src="js/linegraph1.js"></script>
  </body>
</html>
