<?php
include("../includes/common.php");
//setting header to json
header('Content-Type: application/json');
$year =$_REQUEST['year'];
$sid =$_REQUEST['id'];
$AD = 3;
$EV = 2;
$NO = 1;
$NA = 0;

//$year = "2018 - 2019";
//$sid=1;
$studata=$db->prepare("select * from students where st_id = :id");
$studata->bindParam(':id',$sid);
$studata->execute();
$studatadet = $studata -> fetch();

$ft=$db->prepare("select overall_grade, accademic_year from assess_term where st_id =:sid and accademic_year=:year");
$ft->bindParam(':sid', $sid);
$ft->bindParam(':year', $year);
$ft->execute();
$result = $ft->fetchAll(PDO::FETCH_ASSOC);
$count = $ft->rowCount();
for($i=0;$i<$count;$i++)
{
	$ft1 = $db->prepare("select * from students,assess_term where students.id_schools= :id_schools and students.class=:class and students.section= :section and assess_term.accademic_year=:year and students.st_id = assess_term.st_id");
	$ft1->bindParam(':id_schools', $studatadet['id_schools']);
	$ft1->bindParam(':class', $studatadet['class']);
	$ft1->bindParam(':section', $studatadet['section']);
	$ft1->bindParam(':year', $year);
	$ft1->execute();
	$result1 = $ft1->fetchAll(PDO::FETCH_ASSOC);

	$count1 = $ft1->rowCount();
	for($j=0;$j<$count1;$j++)
	{
	   if($result1[$j]['overall_grade']=="AD") $val = 3;
 	   else if($result1[$j]['overall_grade']=="EV") $val = 2;
	   else if($result1[$j]['overall_grade']=="NO") $val = 1;
           else if($result1[$j]['overall_grade']=="NA") $val = 0;	

	    $avg1 = $avg1+$val;
	}
	$avg1 = $avg1/$count1;
	
 	$ft2 = $db->prepare("select * from students,assess_term where students.id_schools= :id_schools and students.class=:class and assess_term.accademic_year=:year and students.st_id = assess_term.st_id");
	$ft2->bindParam(':id_schools', $studatadet['id_schools']);
	$ft2->bindParam(':class', $studatadet['class']);
	$ft2->bindParam(':year', $year);
	$ft2->execute();
	$result2 = $ft2->fetchAll();
	
	$count2 = $ft2->rowCount();
	for($k=0;$k<$count2;$k++)
	{
	   if($result2[$k]['overall_grade']=="AD") $val1 = 3;
 	   else if($result2[$k]['overall_grade']=="EV") $val1 = 2;
	   else if($result2[$k]['overall_grade']=="NO") $val1 = 1;
           else if($result2[$k]['overall_grade']=="NA") $val1 = 0;	

	   $avg2 = $avg2+$val1;
	}
	$avg2 = $avg2/$count2;
	


	$ft3 = $db->prepare("select * from students,assess_term where students.class=:class and assess_term.accademic_year=:year and students.st_id = assess_term.st_id");
	$ft3->bindParam(':class', $studatadet['class']);
	$ft3->bindParam(':year', $year);
	$ft3->execute();
	$result3 = $ft3->fetchAll(PDO::FETCH_ASSOC);

	$count3 = $ft3->rowCount();
	for($l=0;$l<$count3;$l++)
	{
	   if($result3[$l]['overall_grade']=="AD") $val2 = 3;
 	   else if($result3[$l]['overall_grade']=="EV") $val2 = 2;
	   else if($result3[$l]['overall_grade']=="NO") $val2 = 1;
           else if($result3[$l]['overall_grade']=="NA") $val2 = 0;	

	   $avg3 = $avg3+$val2;
	}
	$avg3 = $avg3/$count3;

//print_r($result3);


	if($result[$i]['overall_grade']=="AD") $vals = 3;
 	else if($result[$i]['overall_grade']=="EV") $vals = 2;
	else if($result[$i]['overall_grade']=="NO") $vals = 1;
        else if($result[$i]['overall_grade']=="NA") $vals = 0;	



	$result[$i]['student_skill'] = $vals;
	$result[$i]['class_average'] = $avg1;
	$result[$i]['school_average'] = $avg2;
	$result[$i]['RS_average'] = $avg3;
	//$result[$i]['school_average'] = round($result2[0],2);
	//$result[$i]['average'] = round($result3[0],2);
}

//print_r($data);
//free memory associated with result


//now print the data
echo  json_encode($result);
