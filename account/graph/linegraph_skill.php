<?php 
$year =$_REQUEST['year'];
$sid =$_REQUEST['id'];
?>
<!DOCTYPE html>
<html>
  <head>
    <title>ChartJS - LineGraph</title>
    <style>
      .chart-container {
        width: 640px;
        height: auto;
      }
    </style>
  </head>
  <body>
    <div class="chart-container">
	<input type="hidden" name="year" id="year" value="<?php echo $year;?>">
	<input type="hidden" name="id" id="id" value="<?php echo $sid;?>">
      <canvas id="mycanvas"></canvas>
    </div>
    
    <!-- javascript -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/Chart.min.js"></script>
    <script type="text/javascript" src="js/linegraph_skill.js"></script>
  </body>
</html>
