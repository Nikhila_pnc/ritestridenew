$(document).ready(function(){
	var year = $("#year").val();
        var sid = $("#id").val();

	$.ajax({
		url : "http://webapp.ritestride.in/account/graph/skill_data.php?year="+year+"&id="+sid,
		type : "GET",
		success : function(data){
			console.log(data);

			var term = [];
			var height = [];
			var class_average = [];
			var school_average = [];
			var average = [];
			for(var i in data) {
				term.push("Academic Year : "+data[i].accademic_year);
				height.push(data[i].student_skill);
				class_average.push(data[i].class_average);
				school_average.push(data[i].school_average);
				average.push(data[i].RS_average);
			}

			var chartdata = {
				labels: term,
				datasets: [
					{
						label: "Student Skill",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(59, 89, 152, 0.75)",
						borderColor: "rgba(59, 89, 152, 1)",
						pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
						pointHoverBorderColor: "rgba(59, 89, 152, 1)",
						pointRadius: 5,
						pointHoverRadius: 5,
						data: height
					},
					{
						label: "Class average skill ",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(29, 202, 255, 0.75)",
						borderColor: "rgba(29, 202, 255, 1)",
						pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
						pointHoverBorderColor: "rgba(29, 202, 255, 1)",
						pointRadius: 5,
						pointHoverRadius: 5,
						data: class_average
					},
					 {
						label: "School average skill ",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(211, 72, 54, 0.75)",
						borderColor: "rgba(211, 72, 54, 1)",
						pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
						pointHoverBorderColor: "rgba(211, 72, 54, 1)",
						pointRadius: 5,
						pointHoverRadius: 5,
						data: school_average
					},

					 {
						label: "RS Average skill",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(49, 180, 0)",
						borderColor: "rgba(49, 180, 1)",
						pointHoverBackgroundColor: "rgba(49, 180, 1)",
						pointHoverBorderColor: "rgba(49, 180, 1)",
						pointRadius: 5,
						pointHoverRadius: 5,
						data: average
					}
				]
			};

			var ctx = $("#mycanvas");

			var LineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata

			});
		},
		error : function(data) {

		}
	});
});
