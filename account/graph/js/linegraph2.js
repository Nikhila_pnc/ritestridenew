$(document).ready(function(){
	//alert("Hi");
        var year = $("#year").val();
        var sid = $("#id").val();
        var param = $("#param").val();
	$.ajax({
		url : "http://localhost/Webapp.com/account/graph/data_new_2.php?year="+year+"&id="+sid+"&param="+param,
		type : "POST",
		success : function(data){
		 console.log(data);

			var names = [];
			var Term1height = [];
			var Term2height = [];
			var pTerm1=[];
			var pTerm2=[];

			//var pTerm1 =new Array(data.length).fill(null);
			//var pTerm2 =new Array(data.length).fill(null);

			for(var i in data) {
				names.push(data[i].names);
				Term1height.push(data[i].Term1);
				Term2height.push(data[i].Term2);

				if(data[i].sid == sid)
				{
					
					pTerm1.push(data[i].Term1);
					pTerm2.push(data[i].Term2);
				}
			}

			var chartdata = {
				labels: names,
				datasets: [
					{
						label: "Term1",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "green",
						borderColor: "green",
						pointHoverBackgroundColor: "green",
						pointHoverBorderColor: "green",
						data: Term2height
					},
					{
						label: "Term2",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "#CCC",
						borderColor: "#CCC",
						pointHoverBackgroundColor: "#ccc",
						pointHoverBorderColor: "#ccc",
						data: Term1height
					},
					 {
						label: "Particular student Term1",
						fill: false,
						 lineTension:0.1,
				                 backgroundColor: '#C0392B',
				                 borderColor: '#C0392B',
				                 pointHoverBackgroundColor: '#C0392B',
				                 pointHoverBorderColor: '#C0392B',
				                 pointRadius:10,
						 pointHoverRadius:10,
						 data: pTerm1
					},

					 {
						label: "Particular student Term2",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "#000",
						borderColor: "#000",
						pointHoverBackgroundColor: "#000",
						pointHoverBorderColor: "#000",
						pointRadius:15,
						pointHoverRadius:15,
						data: pTerm2
					}
				]
			};

			var ctx = $("#mycanvas");

			var LineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata,
				options: {
				legend: {
				    display: true,
				    labels: {
					fontColor: '#000'
				    }
				},
				 responsive: true,
				hover: {
				    mode: 'label'
				},
				scales: {
				    xAxes: [{
				     display : false,
           			     autoSkip: false, // don't hide lables while scaling
      maxRotation: 0 // d
				           
				        }],
				    yAxes: [{
				            display: true,
				            ticks: {
					        beginAtZero: true,
				                steps: 15,
				                stepValue: 1,
				                max: 180,

				            }
				        }]
				},
				

			    }
			});
		},
		error : function(data) {

		}
	});
});
