<?php session_start();
include "header.php";

$page="Assessment";
include "sidebar.php";
//list of students
$stu=$db->prepare("select students.*,schools.* from students, schools where students.id_schools = schools.id_schools and students.student_id='".$student_id."'");
$stu->execute();
$details = $stu->fetch();
$st_id=$details['st_id'];
//echo $st_id;
$class =$details['class'];
//$month = date('m')-1;

?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo "> <a href='assessment.php' class='tip-bottom'>".$page;?></a></div>

  <div class="container-fluid"><br>
	<div style="border:1px solid #CCC;width:auto;color:#000;margin-left:10%;margin-right:10%;padding:1px;color:#fff;text-align:center;border-radius:5px" class="bg_rp">
	<h3>Assessment Report</h3>
	</div>
	<br><br>

	<div  style="border:1px solid #CCC;width:auto;color:#000;margin-left:10%;margin-right:10%;padding:10px;color:#000;background-color:#fff;border-radius:5px">
		<h5 style="padding-left:25px">STUDENT NAME : <?php echo $details['st_name'];?> (<?php echo $details['student_id'];?>)
		<br> Gender : <?php echo $details['gender'];?>
		<br><br>School Details  : - <?php echo $details['school_name'];?> , <?php echo $details['school_details'];?>
	<br> Std : <?php echo $details['class'];?> &nbsp;&nbsp;Section : <?php echo $details['section'];?>
	<br><br>Parent Details  : - <?php echo $details['parent_name'];?> 
	<br> Email : <?php echo $details['email'];?> &nbsp;&nbsp;Phone Number : <?php echo $details['phone'];?>
		</h5>
	</div>
	
	<div  style="border:1px solid #CCC;width:auto;color:#000;margin-left:10%;margin-right:10%;padding:1px;color:#000;border-radius:5px;background-color:#fff" >
		<table style="width:100%"><tr><td style="padding-top:12px;width:50%;padding-left:20px">		
		Select
		 &nbsp;&nbsp;&nbsp;<select name="year" id="year"  style="width:auto" required>
		<option value="">Academic Year </option>
		<option value="2018 - 2019">2018 - 2019</option>	
		<option value="2017 - 2018">2017 - 2018</option>			
		<option value="2016 - 2017">2016 - 2017</option>		
		</select></td><td align="left">
		
            <input type="button" class="btn btn-primary" id="showbtn" onclick="return shows('<?php echo $class;?>','<?php echo $st_id;?>','<?php echo $student_id;?>');" value="View Report" style="width:auto;padding:5px"></td></tr></table>
	</div>
	<br><br>
	<div id="assess">
	</div>
	
</div>

  </div>
</div>
<style>
#exTab1 .tab-content {
  color : #000;

  padding : 5px 15px;
}

#exTab2 h3 {
  color :  #000;

  padding : 5px 15px;
}

/* remove border radius for the tab */

#exTab1 .nav-pills > li > a {
  border-radius: 0;
}

/* change border radius for the tab , apply corners on top*/

#exTab3 .nav-pills > li > a {
  border-radius: 4px 4px 0 0 ;
}

#exTab3 .tab-content {
  color : #000;

  padding : 5px 15px;
}

td:hover { 
   background-color: #111;
}
tr:hover td {
    background-color: #333; /* or #000 */
	color:#fff;
}
</style>
<script>
function shows(str,sid,stid) {
var year= document.getElementById("year").value;

	if(year=="")
	{
		alert("Please select year");
		document.getElementById("year").focus();
		return false;
	}
  var xhttp;    
  if (str == "") {
    document.getElementById("assess").innerHTML = "";
    return;
  }
  
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("assess").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "getassess.php?q="+str+"&year="+year+"&id="+sid+"&stid="+stid, true);
  xhttp.send();
}
</script>
<script>
function showgraph(myURL, title, myWidth, myHeight,year,id,param)
{
	var left = (screen.width - myWidth) / 2;
        var top = (screen.height - myHeight) / 4;
        var myWindow = window.open(myURL+"?year="+year+"&id="+id+"&param="+param, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + myWidth + ', height=' + myHeight + ', top=' + top + ', left=' + left);
}

function showskillgraph(myURL, title, myWidth, myHeight,year,id)
{
	var left = (screen.width - myWidth) / 2;
        var top = (screen.height - myHeight) / 4;
        var myWindow = window.open(myURL+"?year="+year+"&id="+id, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + myWidth + ', height=' + myHeight + ', top=' + top + ', left=' + left);
}
</script>
<?php
include "footer.php";
?>

 
