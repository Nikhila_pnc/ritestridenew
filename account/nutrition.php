<?php session_start();
include "header.php";

$page="nutrition";
include "sidebar.php";


?>

<!--main-container-part-->

<!--Action boxes-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo ">".$page;?></div>
  </div>




  <div class="container-fluid">
<h3>Nutrition Activity with Kids: Sugar Free Lemonade with Orange Juice</h3>
<!--Action boxes-->
	<table border=1>
	<tr><td style="font-size:16px;line-space:15px;padding:10px">
		<b>Ingredients</b><br><br>
		<ul><li>1 lemon</li>
		<li>6 Mint leaves</li>
		<li>10 Ice cubes</li>
		<li>1 Tbsp honey</li>
		<li>1/2 cup orange juice</li></ul>
		<b>Utensils</b><br>
		1 Jug</td><td  style="font-size:16px;line-space:15px;padding:10px">
		
		<b>Preparation</b><br><br>
		<ul><li>1 Wash and dry lemon</li>
		<li>2 Slice lemon and put wedges into the jug 3 Crush the mint leaves</li>
		<li>4 Place mint leaves on top of the lemon wedges and gently push them with a spoon</li>
		<li>5 Crush ice cubes</li>
		<li>6 Fill jug with crushed ice and pour honey on top and mix</li>
		<li>7 Pour water and orange juice into the jug</li>
		<li>8 Stir well and serve</li></ul>		
	</td></tr>
	</table>   


<!--End-Action boxes-->    
  </div>
</div>
<?php
include "footer.php";
?>

 
