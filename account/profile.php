<?php session_start();
include "header.php";

$page="Profile";
include "sidebar.php";
$stu=$db->prepare("select students.*,schools.* from students, schools where students.id_schools = schools.id_schools and students.student_id='".$student_id."'");
$stu->execute();
$details = $stu->fetch();
$st_id=$details['st_id'];

//list of schools
$schools=$db->prepare("select * from schools");
$schools->execute();
$rows = $schools->fetchAll();
?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo "> <a href='profile.php'  class='tip-bottom'>".$page;?></a></div>
  </div>


  <div class="container-fluid" style="padding:5%;"><br>
       <div  style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:5%;margin-right:5%;padding:25px">

	 <h4>STUDENT ID : <?php echo $details['student_id'];?><span style="padding-left:65%"><input type="button"  onclick="show('register');" class="btn btn-primary" value="Update"></span></h4>
	<h5 style="padding-left:25px">Name : <?php echo $details['st_name'];?> 
	<br> Gender : <?php echo $details['gender'];?>
	</h5>
 	<h4>School Details </h4>
	<h5 style="padding-left:25px"><?php echo $details['school_name'];?> , <?php echo $details['school_details'];?>
	<br> Std : <?php echo $details['class'];?> &nbsp;&nbsp;Section : <?php echo $details['section'];?></h5>
	<hr>
	<h4>Parent Details </h4>
	<h5 style="padding-left:25px">Name : <?php echo $details['parent_name'];?>
	<br> Email Address : <?php echo $details['email'];?> <br>
	Address : <?php echo $details['address'];?></br>
	Phone Number : <?php echo $details['phone'];?></h5>
	</div>


  </div>




<?php
if($_REQUEST['submit']=="Update")
{
	$stid = $_REQUEST['stid'];	
	$class= $_REQUEST['class'];
	$section= $_REQUEST['section'];
	$school= $_REQUEST['school'];
	$pname= $_REQUEST['pname'];		
	$email=$_REQUEST['email'];
	$phone= $_REQUEST['phone'];
	$address= $_REQUEST['address'];

	
	try{
		$stmt = $db->prepare("UPDATE students SET class =?, section=?, school_details = ?, parent_name = ?, email = ?, phone = ?, address = ? WHERE st_id=?");
		$stmt->execute(array($class,$section,$school,$pname,$email,$phone,$address,$stid));

	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='profile.php';</script>";
}
?>



<div id="register" style="display:none;">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Profile Updation</h3>
        </div><br>
        <div class="widget-content"  style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%;">
          <form action="profile.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
           
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Class :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Class" name="class" id="class" value="<?php echo $details['class'];?>"  required/>
              </div>
            </div>
           <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Section :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Section" name="section" id="section" value="<?php echo $details['section'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  School Details :</label>
              <div class="controls">
		<select id="school" name="school" required>
                <option value="">Select</option>
		    <?php
		        for($i=0;$i<count($rows);$i++){?>
		      <option value="<?php echo $rows[$i]['id_schools'];?>" <?php if($details["id_schools"] == $rows[$i]['id_schools']) echo "selected==selected";?>><?php echo $rows[$i]['school_name'];?></option>
			<?php }?>
               </select>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Parent Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Parent Name" name="pname" id="pname" value="<?php echo $details['parent_name'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Email Address :</label>
              <div class="controls">
                <input type="email" class="" placeholder="Email"  name="email" id="email" value="<?php echo $details['email'];?>" required/>
              </div>
            </div>
          
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Phone Number :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Phone Number" name="phone" id="phone" value="<?php echo $details['phone'];?>"  required/>
		 <input type="hidden" class="" placeholder="" name="stid" id="stid" value="<?php echo $details['st_id'];?>"  required/>			
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Address :</label>
              <div class="controls">
		<textarea class="" placeholder="Address" name="address" id="address"><?php echo $details['address'];?></textarea>
              </div>
            </div>
	  
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Update" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>

</div>
<script>
var divs = ["register"];
var visibleDivId = null;
function show(divId) {
  if(visibleDivId === divId) {
    //visibleDivId = null;
  } else {
    visibleDivId = divId;
  }
  hideNonVisibleDivs();
}
function hideNonVisibleDivs() {
  var i, divId, div;
  for(i = 0; i < divs.length; i++) {
    divId = divs[i];
    div = document.getElementById(divId);
    if(visibleDivId === divId) {
      div.style.display = "block";
    } else {
      div.style.display = "none";
    }
  }
}
</script>
<?php
include "footer.php";
?>

 
