<?php session_start();
//include("checkSession.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Rite Stride Pvt Ltd</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />

    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
	
	<!-- css files -->
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href="css/style.css" rel='stylesheet' type='text/css' /><!-- custom css -->
	<link href="css/css_slider.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/font-awesome.min.css" rel="stylesheet"><!-- fontawesome css -->
	<!-- //css files -->
	
	<!-- google fonts -->
	<link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet">
	<!-- //google fonts -->
	
</head>
<body>

<!-- header -->
<header>
	<div class="top-head container">
<div class="ml-auto text-left left-p">
		<div id="logo">
				<h1> <a href="http://ritestride.in/" target="_blank"><img src="images/logo.png" alt=""> </a></h1>
			</div>	
		</div>
		<div class="ml-auto text-right right-p">
			<ul>
				<li>
					<span class="fa fa-envelope-open"></span> <a href="mailto:info@ritestride.in" style="font-weight:bold;color:#fff">info@ritestride.in</a> </li>
			</ul>
		</div>
	</div>
	
</header>
<!-- //header -->

<!-- banner -->
<div class="banner" id="home">
	<div class="layer ">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 banner-text-w3pvt">
					<!-- banner slider-->
					<div class="csslider infinity" id="slider1">
						<input type="radio" name="slides" checked="checked" id="slides_1" />
						<input type="radio" name="slides" id="slides_2" />
						<ul class="banner_slide_bg">
							<li>
								<div class="container-fluid">
									<div class="w3ls_banner_txt">
										<h3 class="b-w3ltxt text-capitalize mt-md-4">Mission</h3>
										<p class="w3ls_pvt-title my-3" style="color:#fff;text-align:justify;font-weight:bold">At Rite Stride we have taken the latest western concepts and applied them to our own Indian teaching methods. This, we believe has created the best possible physical and nutrition education program. We continually seek to update and improve on the educational experience. We are here to raise the bar and make a difference!</p>
									</div>
								</div>
							</li>
							<li>
								<div class="container-fluid">
									<div class="w3ls_banner_txt">
										<h3 class="b-w3ltxt text-capitalize mt-md-4">Vision</h3>
										<p class="w3ls_pvt-title my-3"  style="color:#fff;text-align:justify;font-weight:bold">Rite Stride's vision to create a well-rounded child, who is active, confident and healthy.
Rite Stride kids are physically fit and mentally alert.
They are ready to take on the world!</p>
									</div>
								</div>
							</li>
							
						</ul>
						<div class="navigation">
							<div>
								<label for="slides_1"></label>
								<label for="slides_2"></label>
							</div>
						</div>
					</div>
					<!-- //banner slider-->
				</div>
				<div class="col-lg-5 col-md-8 px-lg-3 px-0">
					<div class="banner-form-w3 ml-lg-5">
						<div class="padding">

<?php
if($_REQUEST['submit']=="Submit")
{
	$db = new PDO('mysql:host=localhost;dbname=webapp_ritestride;charset=utf8mb4', 'rs_webapp', 'rs_webapp_aug19');

	 $stmt=$db->prepare("select * from students where email = :email");
	 $stmt->bindParam(':email', $_REQUEST['email']);        
	 $stmt->execute();
	 $details = $stmt -> fetch();  
	 $count = $stmt->rowCount();
	 if ($count>0)
	 {        
	    $pwd= $details['password'];
	    $pname = $details['st_name'];
	    $stid= $details['student_id'];
            $globalemail= "info@ritestride.in";
	    //for registering person
			 $to = $_REQUEST['email'];
			 $subject = "Forgot Password - Rite Stride";
			 $message = "Dear ".ucfirst(strtolower($pname))."<br><br> Thank you for contacting us. Your kid's Id for login is : ".$stid." and your password is :".$pwd."<br><br> Thanks. </br>RiteStride Team.";
				
			 $headers = "From:".$globalemail . "\r\n";
			 $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			 mail($to,$subject,$message,$headers);
	    $res="Email has sent to registered email address. <a href='index.php' style='color:#111'>Please click here to go back.</a>";
	 }
	else $res="Please enter registered email address";
}
?>
							<!-- banner form -->
							<form action="forgotpwd.php" method="post">
							<a href="index.php"><div id="tclose">X</div></a>    
							<div id="forgotpwd">
							<h5 class="mb-3">Forgot Password</h5>
							<div class="form-style-w3ls">
							<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
							<input placeholder="Registered Email Address" name="email" type="text" required>
							<input  type="submit" name="submit" value="Submit">
							</div>
							</div>
							</form>
							<!-- //banner form -->
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<style>
#tclose{
  position:absolute;
  background:black;
  color:white;
  right:25px;
  top:225px;
  border-radius:50%;
  width:30px;
  height:30px;
  line-height:30px;
  text-align:center;
  font-size:8px;
  font-weight:bold;
  font-family:'Arial Black', Arial, sans-serif;
  cursor:pointer;
  box-shadow:0 0 10px 0 #000;
}
</style>
<!-- //banner -->
<?php 
include("footer.php");
?>
