<!-- copyright -->
<section class="copyright">
	<div style="width:95%;padding:10px">
		<div class="row bottom">		
			<div class="copy-right" style="width:50%;text-align:left;padding-left:50px">
				<p style="color:#fff;">© <?php echo date("Y");?> <a href="http://ritestride.in" target="_blank" style="color:#fff">Rite Stride Pvt Ltd</a>. All rights reserved </p>
				<p style="color:#fff;font-size:10px"> Design by
					<a href="http://w3layouts.com" target="_blank" style="color:#fff;"> W3layouts.</a>
				</p>
			</div>
			<div style="width:50%;text-align:right">
				<a href="https://www.jivass.com" target="_blank"><img src="images/jivass.png"></a>		
			</div>
		</div>
	</div>
</section>
<!-- //copyright -->

<!-- move top 
<div class="move-top text-right">
	<a href="#home" class="move-top"> 
		<span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
	</a>
</div>
<>!-- move top -->

</body>
</html>
