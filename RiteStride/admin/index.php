<?php session_start();
include "header.php";
include_once ('./classes/paginator.php');

$page="";
include "sidebar.php";


?>

<!--main-container-part-->

<!--Action boxes-->

<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->
<h3 style="padding-left:2%">Welcome to Dashboard</h3>
<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb" style="border-radius:25px"> <a href="students_list.php"> <i class="icon-user"></i> Students </a> </li>
        <li class="bg_lp" style="border-radius:25px"> <a href="forms.php"> <i class="icon-th-list"></i> Forms</a> </li>
      </ul>
    </div>
<!--End-Action boxes-->    
</div>
</div>
<?php
include "footer.php";
?>

 
