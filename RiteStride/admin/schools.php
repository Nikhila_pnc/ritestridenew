<?php session_start();
include "header.php";

$page="schools";
include "sidebar.php";


?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='school_list.php' title='School List' class='tip-bottom'>".$page;?></a><a href='#' title='' style="cursor:none" class='tip-bottom'>add</a></div>
  </div>
<?php
if($_REQUEST['submit']=="Submit")
{
	$schoolname = $_REQUEST['school'];	
	$details= $_REQUEST['details'];

	
	try{
		$stmt = $db->prepare("INSERT INTO schools(school_name,school_details) VALUES (:name,:det)");
		$stmt->bindParam(':name', $schoolname);
		$stmt->bindParam(':det', $details);
		$stmt->execute();
	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='school_list.php';</script>";
}
?>


<br><br>
<div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>School Details</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:50%;background-color:#fff;color:#000;margin-left:5%">
          <form action="schools.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
           
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  School Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="School Name" name="school" id="school" value="<?php echo $_REQUEST['school'];?>"  required/>
              </div>

	   </div>
	   <div class="control-group">
              <label class="control-label">  School Details :</label>
	   <div class="controls">
		<textarea name="details" id="details" placeholder="School Details"><?php echo $_REQUEST['details'];?></textarea>
              </div>
            </div>
	  
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Submit" class="btn btn-success" /> <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>

</div>
<script>
function closed()
{
	window.location.href="school_list.php";
}
</script>
<?php
include "footer.php";
?>

 
