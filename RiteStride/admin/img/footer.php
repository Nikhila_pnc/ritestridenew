<?php
/**
 * Footer content
 * @since Event Star 1.0.0
 *
 * @param null
 * @return null
 *
 */
if ( ! function_exists( 'event_star_footer' ) ) :

    function event_star_footer() {

        global $event_star_customizer_all_values;
	    ?>
        <div class="clearfix"></div>
        <footer class="site-footer">
            <?php
            $footer_column = 0;
            if(is_active_sidebar('footer-col-one') ){
	            $footer_column++;
            }
            if(is_active_sidebar('footer-col-two') ){
	            $footer_column++;
            }
            if(is_active_sidebar('footer-col-three') ){
	            $footer_column++;
            }
            if(is_active_sidebar('footer-col-four') ){
	            $footer_column++;
            }
            if( 0 !=$footer_column ) {
                ?>
                <div class="footer-columns at-fixed-width">
                    <div class="container">
                        <div class="row">
			                <?php
			                if ( 2 == $footer_column ){
				                $footer_top_col = 'col-sm-6 init-animate';
			                }
                            elseif ( 3 == $footer_column ){
				                $footer_top_col = 'col-sm-4 init-animate';
			                }
                            elseif ( 4 == $footer_column ){
				                $footer_top_col = 'col-sm-3 init-animate';
			                }
			                else{
				                $footer_top_col = 'col-sm-12 init-animate';
			                }
			                $footer_top_col .= ' zoomIn';
			                if (is_active_sidebar('footer-col-one')) : ?>
                                <div class="footer-sidebar <?php echo esc_attr($footer_top_col); ?>">
					                <?php dynamic_sidebar('footer-col-one'); ?>
                                </div>
			                <?php endif;
			                if (is_active_sidebar('footer-col-two')) : ?>
                                <div class="footer-sidebar <?php echo esc_attr($footer_top_col); ?>">
					                <?php dynamic_sidebar('footer-col-two'); ?>
                                </div>
			                <?php endif;
			                if (is_active_sidebar('footer-col-three')) : ?>
                                <div class="footer-sidebar <?php echo esc_attr($footer_top_col); ?>">
					                <?php dynamic_sidebar('footer-col-three'); ?>
                                </div>
			                <?php endif;
			                if (is_active_sidebar('footer-col-four')) : ?>
                                <div class="footer-sidebar <?php echo esc_attr($footer_top_col); ?>">
					                <?php dynamic_sidebar('footer-col-four'); ?>
                                </div>
			                <?php endif; ?>
                        </div>
                    </div><!-- bottom-->
                </div>
                <div class="clearfix"></div>
                <?php
            }
            ?>
            <div class="container" style="background-color:#191810;width:100%;"><br>
             			<div style="float:left;">
                                <?php
                                if( isset( $event_star_customizer_all_values['event-star-footer-copyright'] ) ): ?>
                                    <p class="at-display-inline-block" >
                                        <?php //echo wp_kses_post( $event_star_customizer_all_values['event-star-footer-copyright'] ); ?>
                                    <a target="_blank" href="https://www.iitm.ac.in/"> <img  src="http://wordpresstest.jivass.com/Aero-Conference/NationalConference/wp-content/uploads/2019/05/iitmlogo1.png" width="50" height="50"> </a><p style = "float:right">Copyright © 2019<br> Indian Institute of Technology Madras </p>
                                    </p>
                                <?php endif; ?>
				</div><div  style="float:right">
                                <div class="site-info at-display-inline-block text-right">
				<a target="_blank" href="https://jivass.com/">  <img src="http://wordpresstest.jivass.com/Aero-Conference/NationalConference/wp-content/uploads/2019/05/jiv11-1.jpg" height="180"> </a>
			      <?php //echo wp_kses_post( $event_star_customizer_all_values['event-star-footer-copyright'] ); ?>
                                   
                             <!-- <div class="site-info" style="font-size:9px">
                                    Template by <a href="http://www.acmethemes.com/" rel="designer" style="font-size:10px">Acme Themes</a> 
			      </div>
				-->                    
                                </div><!-- .site-info -->
				</td></tr></table>
         </div>
               
        </footer>
    <?php
    }
endif;
add_action( 'event_star_action_footer', 'event_star_footer', 10 );

/**
 * Page end
 *
 * @since Event Star 1.0.0
 *
 * @param null
 * @return null
 *
 */
if ( ! function_exists( 'event_star_page_end' ) ) :

    function event_star_page_end() {
	    global $event_star_customizer_all_values;
	    $event_star_booking_form_title = $event_star_customizer_all_values['event-star-popup-widget-title'];
	    ?>
        <!-- Modal -->
        <div id="at-shortcode-bootstrap-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
					    <?php
					    if( !empty( $event_star_booking_form_title ) ){
						    ?>
                            <h4 class="modal-title"><?php echo esc_html( $event_star_booking_form_title ); ?></h4>
						    <?php
					    }
					    ?>
                    </div>
				    <?php
				    if( is_active_sidebar( 'popup-widget-area' ) ) :
					    echo "<div class='modal-body'>";
					    dynamic_sidebar( 'popup-widget-area' );
					    echo "</div>";
				    endif;
				    ?>
                </div><!--.modal-content-->
            </div>
        </div><!--#at-shortcode-bootstrap-modal-->

        </div><!-- #page -->
    <?php
    }
endif;
add_action( 'event_star_action_after', 'event_star_page_end', 10 );
