<?php session_start();
include "header.php";

$page="units";
include "sidebar.php";

$quesid =$_REQUEST['id'];

	$comp=$db->prepare("select * from unit_questions where id_unit_ques = :id");
	$comp->bindParam(':id',$quesid);
	$comp->execute();
	$compdet = $comp -> fetch();
	$questions= $compdet['questions'];
	$id_units=$compdet['id_units'];


//list of schools
$units=$db->prepare("select * from units");
$units->execute();
$rows = $units->fetchAll();

?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='unit_list.php' title='Unit List' class='tip-bottom'>".$page;?></a><a href='questions.php?id=<?php echo $id_units;?>' title='Question List'  class='tip-bottom'>questions</a><a href='#' title='' style="cursor:none" class='tip-bottom'>edit</a></div>
  </div>
<?php
if($_REQUEST['submit']=="Update")
{
	$ques = $_REQUEST['question'];	
	$unit= $_REQUEST['unit'];
	$quesid = $_REQUEST['quesid'];	

	try{
		$stmt = $db->prepare("UPDATE unit_questions SET questions =?, id_units=? WHERE id_unit_ques=?");
		$stmt->execute(array($ques,$unit,$quesid));	
	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='questions.php?id=$unit';</script>";
}
?>


<br><br>
<div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Questions</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:50%;background-color:#fff;color:#000;margin-left:5%">
          <form action="ques_edit.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
           
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Question :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Question" name="question" id="question" value="<?php echo $questions;?>"  required/>
              </div>

	   </div>
	   <div class="control-group">
              <label class="control-label">  Unit :</label>
	   <div class="controls">
		<select id="unit" name="unit" required>
                <option value="">Select</option>
		     <?php for($i=0;$i<count($rows);$i++){?>
		      <option value="<?php echo $rows[$i]['id_units'];?>" <?php if($compdet["id_units"] == $i+1) echo "selected==selected";?>><?php echo $rows[$i]['unit_name'];?></option>
			<?php }?>
               </select>
              </div>
            </div>
	                  <input type="hidden" name="quesid" value=<?php echo $quesid;?>>		
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Update" class="btn btn-success" /> <input type="button" name="close" value="Close" onclick="closed(<?php echo $id_units;?>);" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>

</div>
<script>
function closed(id)
{
	window.location.href="questions.php?id="+id;
}
</script>
<?php
include "footer.php";
?>

 
