<!--Footer-part-->

<div class="row">
  <div id="footer" class="span6" style="float:left">  © <?php echo date("Y");?> <a href="http://ritestride.in/" target="_blank">Rite Stride Pvt Ltd</a> </div>
<div style="float:right"><a href="https://www.jivass.com" target="_blank"><img src="img/jivass.png"></a></div>
</div>

<!--end-Footer-part-->

<script src="js/excanvas.min.js"></script> 
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.flot.min.js"></script> 
<script src="js/jquery.flot.resize.min.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/fullcalendar.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.dashboard.js"></script> 
<script src="js/jquery.gritter.min.js"></script> 
<script src="js/matrix.interface.js"></script> 
<script src="js/matrix.chat.js"></script> 
<script src="js/jquery.validate.js"></script> 
<script src="js/matrix.form_validation.js"></script> 
<script src="js/jquery.wizard.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/matrix.popover.js"></script> 
<script src="js/jquery.dataTables.min.js"></script> 
<script src="js/matrix.tables.js"></script> 

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
<script>
var newwindow;
function createPop(type)
{    
	var url = "register.php?type="+type;
	var width=screen.width/4;
	//alert(width);
   newwindow=window.open(url,"Register",'width=700,height=450,toolbar=0,menubar=0,location=0,top=170,scrollbar=0,left='+width);  
   if (window.focus) {newwindow.focus()}
}
function changesettings(url,companyid)
{
	var url = url+"?cmpid="+companyid;
	var width=screen.width/4;
	//alert(width);
   newwindow=window.open(url,"Settings",'width=700,height=450,toolbar=0,menubar=0,location=0,top=170,scrollbar=0,left='+width);  
   if (window.focus) {newwindow.focus()}
}
</script>
</body>
</html>
