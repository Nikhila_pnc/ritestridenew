<?php session_start();
include "header.php";

$page="units";
include "sidebar.php";
$no=$_REQUEST['no'];
$id=$_REQUEST['id'];

?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='unit_list.php' title='Unit List' class='tip-bottom'>".$page;?></a><a href='questions.php?id=<?php echo $id;?>' title='Question List'  class='tip-bottom'>questions</a><a href='#' title='' style="cursor:none" class='tip-bottom'>add</a></div>
  </div>
<?php
if($_REQUEST['submit']=="Submit")
{
	echo $no = $_REQUEST['no'];	
	$id= $_REQUEST['id'];

	try{
		for($i=1;$i<=$no;$i++)
		{
			$ques=$_REQUEST['question'.$i];
			$stmt = $db->prepare("INSERT INTO unit_questions(questions, id_units) VALUES (:ques,:id)");
			$stmt->bindParam(':ques', $ques);
			$stmt->bindParam(':id', $id);
			$stmt->execute();
		}
	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='questions.php?id=$id';</script>";
}
?>


<br><br>
<div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Question</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:50%;background-color:#fff;color:#000;margin-left:5%">
          <form action="ques_add.php" method="post" class="form-horizontal" enctype="multipart/form-data">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
           
	      <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  How many Questions :</label>
              <div class="controls">
                <select id="no" name="no"  onchange="ques(<?php echo $id;?>);" required>
		    <?php
		        for($i=1;$i<=10;$i++){?>
		      <option value="<?php echo $i;?>" <?php if($no ==$i) echo "selected=selected";?>> <?php echo $i;?></option>
		<?php } ?>
               </select>
              </div>
            </div>
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Question :</label>
              <div class="controls">
		<?php if($no==""){?>
                <input type="text" class="" placeholder="Question" name="question1" id="question1" value="<?php echo $_REQUEST['question1'];?>"  required/>
		<?php }else{
			for($i=1;$i<=$no;$i++){?>
		          <input type="text" class="" placeholder="Question <?php echo $i;?>" name="question<?php echo $i;?>" id="question<?php echo $i;?>" value="<?php echo $_REQUEST['question'.$i];?>"  required/><br><br>
		<?php }}?>
              </div>
	                  <input type="hidden" name="id" value=<?php echo $id;?>>		
	   </div>

	  <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Submit" class="btn btn-success" />  <input type="button" name="close" value="Close" onclick="closed(<?php echo $id;?>);" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
</div>
</div>
<script>
function ques(id)
{
	var no=document.getElementById("no").value; 
	window.location.href="ques_add.php?id="+id+"&no="+no;
}
function closed(id)
{
	window.location.href="questions.php?id="+id;
}
</script>
<?php
include "footer.php";
?>

 
