<?php session_start();
include "header.php";
include_once ('./classes/paginator.php');
$instance = new dbfunctions;
$page="Forms";
include "sidebar.php";
$trans=$_REQUEST['trans'];
//list of students
$stu=$db->prepare("select * from students");
$stu->execute();
$row = $stu->fetchAll();

//list of schools
$schools=$db->prepare("select * from schools");
$schools->execute();
$rows = $schools->fetchAll();


?>

<!--main-container-part-->

<!--Action boxes-->

<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">


   <div class="quick-actions_homepage">
     <h3 style="text-align:left">List of Forms</h3>
      <ul class="quick-actions">
        <li class="bg_rg" style="border-radius:25px"> <a href="#register"  onclick="show('register');"> <i class="icon-user"></i> Student Registration </a> </li>
        <li class="bg_rp" style="border-radius:25px"> <a href="assessment.php" > <i class="icon-certificate"></i>Assessment</a> </li>
       </ul>
     </div>

   </div>
<!--End-Action boxes-->   
<?php

if($_REQUEST['submit']=="Register")
{
		 $res=$instance->registration($_REQUEST); 
	        echo "<script  language='javascript'>window.location='students_list.php';</script>";
}

?>

<div class="container-fluid">
<!--Registration of students-->
  <div id="register" <?php if($trans==1){?>style="display:block;width:100%;" <?php } else{?>style="display:none;width:100%;"<?php }?>>
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Register Students</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%">
          <form action="forms.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Student Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Student Name" name="stname" id="stname" value="<?php echo $_REQUEST['stname'];?>"  required/>
              </div>
            </div>
 	    <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Gender :</label>
              <div class="controls">
                  <input type="radio" name="gender" value="M" required> Male <input type="radio" name="gender" value="F" required> Female
              </div>
            </div>
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Class :</label>
              <div class="controls">
                <select id="class" name="class" required>
                <option value="">Select</option>
		<option value="Pre-KG" >Pre-KG</option>
		<option value="LKG" >LKG</option>
		<option value="UKG">UKG</option>
		    <?php
		        for($i=1;$i<=12;$i++){
		      echo '<option value="'.$i.'">'.$i.'</option>';}?>
               </select>
              </div>
            </div>
           <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Section :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Section" name="section" id="section" value="<?php echo $_REQUEST['section'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  School  :</label>
              <div class="controls">
		<select id="school" name="school" required>
                <option value="">Select</option>
		    <?php
		        for($i=0;$i<count($rows);$i++){
		      echo '<option value="'.$rows[$i]['id_schools'].'">'.$rows[$i]['school_name'].'</option>';}?>
               </select>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Parent Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Parent Name" name="pname" id="pname" value="<?php echo $_REQUEST['pname'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Email Address :</label>
              <div class="controls">
                <input type="email" class="" placeholder="Email"  name="email" id="email" value="<?php echo $_REQUEST['email'];?>" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Password :</label>
              <div class="controls">
                <input type="password"  class="" placeholder="Enter Password" name="password" id="password" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Phone Number :</label>
              <div class="controls">
                <input type="number" class="" placeholder="Phone Number" name="phone" id="phone" value="<?php echo $_REQUEST['phone'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Address :</label>
              <div class="controls">
		<textarea class="" placeholder="Address" name="address" id="address"><?php echo $_REQUEST['address'];?></textarea>
              </div>
            </div>
	  
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Register" class="btn btn-success" />	<input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
	<span style="color:red">*</span> Mandatory
        </div>
      </div>
<!--Registration of students-->


  </div>
</div>
<script>
var divs = ["register"];
var visibleDivId = null;
function show(id) {
  var i, divId, div;
  for(i = 0; i < divs.length; i++) {
    divId = divs[i];
    div = document.getElementById(divId);
	if(id === divId) {
	      div.style.display = "block"; 
	    } else {
	      div.style.display = "none";
	    }
   }
}

</script>


</script>
<script>
function closed()
{
	window.location.href="forms.php";
}
</script>
<style>
input[type=text],input[type=email],input[type=number],input[type=password],select,textarea{
max-width:100%;
}

</style>
<?php
include "footer.php";
?>

 
