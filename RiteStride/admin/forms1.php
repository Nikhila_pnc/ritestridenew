<?php session_start();
include "header.php";
include_once ('./classes/paginator.php');
$instance = new dbfunctions;
$page="Forms";
include "sidebar.php";

//list of students
$stu=$db->prepare("select * from students");
$stu->execute();
$row = $stu->fetchAll();

//list of fiteness assessment fields
$fitness=$db->prepare("select * from fitness");
$fitness->execute();

//list of skills assessment fields
$skills=$db->prepare("select * from skill");
$skills->execute();
?>

<!--main-container-part-->

<!--Action boxes-->

<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">


   <div class="quick-actions_homepage">
     <h3 style="text-align:left">List of Forms</h3>
      <ul class="quick-actions">
        <li class="bg_lb" > <a href="#register"  onclick="show('register');"> <i class="icon-user"></i> Student Registration </a> </li>
        <li class="bg_dg"> <a href="#fitness" onclick="show('fitness');"> <i class="icon-medkit"></i>Fitness Assessment</a> </li>
  	<li class="bg_lr"> <a href="#skills" onclick="show('skills');"> <i class="icon-pencil"></i> Skills Assessment </a> </li>
      </ul>
     </div>

 <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lv" > <a href="#remedy" onclick="show('remedy');"> <i class="icon-key"></i> Remedial Actions  </a> </li>
        <li class="bg_dy"> <a href="#grade" onclick="show('grade');"> <i class="icon-certificate"></i> Grade</a> </li>
      </ul>
     </div>
 
   </div>
<!--End-Action boxes-->   
<?php

if($_REQUEST['submit']=="Register")
{
		 $res=$instance->registration($_REQUEST); 
	       echo "<script  language='javascript'>window.location='forms.php';</script>";
}
if($_REQUEST['add']=="Add")
{
		 $res=$instance->fitness($_REQUEST); 
	       echo "<script  language='javascript'>window.location='forms.php';</script>";
}
if($_REQUEST['add1']=="Add")
{
		 $res=$instance->skills($_REQUEST); 
	       echo "<script  language='javascript'>window.location='forms.php';</script>";
}
if($_REQUEST['add2']=="Add")
{
		 $res=$instance->remedies($_REQUEST); 
	       echo "<script  language='javascript'>window.location='forms.php';</script>";
}
if($_REQUEST['add3']=="Add")
{
		 $res=$instance->grade($_REQUEST); 
	       echo "<script  language='javascript'>window.location='forms.php';</script>";
}
?>

<div class="container-fluid">
<!--Registration of students-->
  <div id="register" style="display:none;100%;">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Register Students</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%">
          <form action="forms.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Student Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Student Name" name="stname" id="stname" value="<?php echo $_REQUEST['stname'];?>"  required/>
              </div>
            </div>
 	    <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Gender :</label>
              <div class="controls">
                  <input type="radio" name="gender" value="M" required> Male <input type="radio" name="gender" value="F" required> Female
              </div>
            </div>
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Class :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Class" name="class" id="class" value="<?php echo $_REQUEST['class'];?>"  required/>
              </div>
            </div>
           <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Section :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Section" name="section" id="section" value="<?php echo $_REQUEST['section'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  School Details :</label>
              <div class="controls">
		<textarea class="" placeholder="School Details" name="school" id="school"><?php echo $_REQUEST['school'];?></textarea>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Parent Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Parent Name" name="pname" id="pname" value="<?php echo $_REQUEST['pname'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Email Address :</label>
              <div class="controls">
                <input type="email" class="" placeholder="Email"  name="email" id="email" value="<?php echo $_REQUEST['email'];?>" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Password :</label>
              <div class="controls">
                <input type="password"  class="" placeholder="Enter Password" name="password" id="password" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Phone Number :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Phone Number" name="phone" id="phone" value="<?php echo $_REQUEST['phone'];?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Address :</label>
              <div class="controls">
		<textarea class="" placeholder="Address" name="address" id="address"><?php echo $_REQUEST['address'];?></textarea>
              </div>
            </div>
	  
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Register" class="btn btn-success" />	<input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
<!--Registration of students-->


<!--Fitness Assessment of students-->
  <div  id="fitness" style="display:none;">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h3>Fitness Assessment Form</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%;">
          <form action="forms.php" method="post" class="form-horizontal" >
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
	    <div class="control-group">
              <label class="control-label"  style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span>  Student ID :</label>
              <div class="controls">
		<select id="studentid" name="studentid" required>
                <option value="">Select</option>
		    <?php
		        for($i=0;$i<count($row);$i++){
		      echo '<option value="'.$row[$i]['st_id'].'">'.$row[$i]['student_id'].'</option>';}?>
               </select>
              </div>
            </div>
        <?php  while ($row1 = $fitness->fetch(PDO::FETCH_ASSOC)) {?>
            <div class="control-group" > 
              <label class="control-label" style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span>  <?php echo $row1['fitness_field'];?></label>
              <div class="controls">
		<select name="Score<?php echo $row1['fitness_id'];?>" id="Score<?php echo $row1['fitness_id'];?>"  style="width:auto" required>
		<option value="">Score</option>
		<option value="1">1</option>	
		<option value="2">2</option>			
		<option value="3">3</option>		
		<option value="4">4</option>	
		<option value="5">5</option>			
		</select>
		&nbsp;&nbsp;
		<select name="Grade<?php echo $row1['fitness_id'];?>" id="Grade<?php echo $row1['fitness_id'];?>" style="width:auto"  required>
		<option value="">Grade</option>
		<option value="A">A</option>	
		<option value="B">B</option>			
		<option value="C">C</option>		
		<option value="D">D</option>		
		</select>
		&nbsp;&nbsp;<textarea  placeholder="Description" name="Des<?php echo $row1['fitness_id'];?>" id="Des<?php echo $row1['fitness_id'];?>" required></textarea>
              </div>
            </div>
	    <?php }?>
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="add" value="Add" class="btn btn-success" />  <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>

<!--Fitness Assessment of students-->


<!--Skills Assessment of students-->
 <div  id="skills" style="display:none;">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h3>Skill Assessment Form</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%;">
          <form action="forms.php" method="post" class="form-horizontal" >
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
	    <div class="control-group">
              <label class="control-label"  style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span>  Student ID :</label>
              <div class="controls">
		<select id="studentid" name="studentid" required>
                <option value="">Select</option>
		    <?php
		        for($i=0;$i<count($row);$i++){
		      echo '<option value="'.$row[$i]['st_id'].'">'.$row[$i]['student_id'].'</option>';}?>
               </select>
              </div>
            </div>
        <?php  while ($row2 = $skills->fetch(PDO::FETCH_ASSOC)) {?>
            <div class="control-group" > 
              <label class="control-label" style="width:30%;padding-right:30px;"><span style="color:red">*</span>  <?php echo $row2['skill_name'];?> : </label>
              <div class="controls">
		<select name="Grade<?php echo $row2['skill_id'];?>" id="Grade<?php echo $row2['skill_id'];?>" style="width:auto"  required>
		<option value="">Grade</option>
		<option value="A">A</option>	
		<option value="B">B</option>			
		<option value="C">C</option>		
		<option value="D">D</option>		
		</select>

              </div>
            </div>
	    <?php }?>
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="add1" value="Add" class="btn btn-success" />  <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
<!--Skills Assessment of students-->

<!--Remedial Action of students-->
 <div  id="remedy" style="display:none;">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h3>Remedial Action</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%;">
          <form action="forms.php" method="post" class="form-horizontal" >
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
	    <div class="control-group">
              <label class="control-label"  style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span>  Student ID :</label>
              <div class="controls">
		<select id="studentid" name="studentid" required>
                <option value="">Select</option>
		    <?php
		        for($i=0;$i<count($row);$i++){
		      echo '<option value="'.$row[$i]['st_id'].'">'.$row[$i]['student_id'].'</option>';}?>
               </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"  style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span>  Anaerobic Capacity :</label>
              <div class="controls">
		<textarea class="" placeholder=" Anaerobic Capacity " name="anaerobic" id="anaerobic"><?php echo $_REQUEST['anaerobic'];?></textarea>
              </div>
            </div>
	   <div class="control-group">
              <label class="control-label"  style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span> Flexibility :</label>
              <div class="controls">
		<textarea class="" placeholder=" Flexibility " name="flexibility" id="flexibility"><?php echo $_REQUEST['flexibility'];?></textarea>
              </div>
            </div>
	   <div class="control-group">
              <label class="control-label"  style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span> Explosive Strength <br>(Lower Body) :</label>
              <div class="controls">
		<textarea class="" placeholder="Explosive Strength (Lower Body)" name="explosive_lb" id="explosive_lb"><?php echo $_REQUEST['explosive_lb'];?></textarea>
              </div>
            </div>
  	   <div class="control-group">
              <label class="control-label"  style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span> Explosive Strength <br>(Upper Body) :</label>
              <div class="controls">
		<textarea class="" placeholder="Explosive Strength (Upper Body)" name="explosive_ub" id="explosive_ub"><?php echo $_REQUEST['explosive_ub'];?></textarea>
              </div>
            </div>
      
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="add2" value="Add" class="btn btn-success" />  <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
<!--Remedial Action of students-->

<!--grade of students-->
 <div  id="grade" style="display:none;">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h3> Grade Form</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%;">
          <form action="forms.php" method="post" class="form-horizontal" >
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
	    <div class="control-group">
              <label class="control-label"  style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span>  Student ID :</label>
              <div class="controls">
		<select id="studentid" name="studentid" required>
                <option value="">Select</option>
		    <?php
		        for($i=0;$i<count($row);$i++){
		      echo '<option value="'.$row[$i]['st_id'].'">'.$row[$i]['student_id'].'</option>';}?>
               </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"  style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span> Overall Grade :</label>
              <div class="controls">
		<select name="Grade" id="Grade" style="width:12%"  required>
		<option value="">Grade</option>
		<option value="A">A</option>	
		<option value="B">B</option>			
		<option value="C">C</option>		
		<option value="D">D</option>		
		</select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" style="width:30%;padding-right:30px;padding-bottom:20px"><span style="color:red">*</span>  TQ Score :</label>
              <div class="controls">
		<input type="text" class="" placeholder="TQ Score" name="tqscore" id="tqscore" value="<?php echo $_REQUEST['tqscore'];?>">
              </div>
            </div>
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="add3" value="Add" class="btn btn-success" />  <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
<!--Skills Assessment of students-->

  </div>
</div>
<script>
var divs = ["register", "fitness", "skills", "remedy","grade"];
var visibleDivId = null;
function show(divId) {
  if(visibleDivId === divId) {
    //visibleDivId = null;
  } else {
    visibleDivId = divId;
  }
  hideNonVisibleDivs();
}
function hideNonVisibleDivs() {
  var i, divId, div;
  for(i = 0; i < divs.length; i++) {
    divId = divs[i];
    div = document.getElementById(divId);
    if(visibleDivId === divId) {
      div.style.display = "block";
    } else {
      div.style.display = "none";
    }
  }
}

</script>
<script>
function closed()
{
	window.location.href="forms.php";
}
</script>
<style>
input[type=text],input[type=email],input[type=password],select,textarea{
max-width:100%;
}

</style>
<?php
include "footer.php";
?>

 
