//<meta charset="utf-8"/>
$(document).ready(function(){
        var year = $("#year").val();
        var sid = $("#id").val();
        var param = $("#param").val();
	$.ajax({
		url : "http://test.jivass.com/RiteStride/account/graph/data.php?year="+year+"&id="+sid+"&param="+param,
		type : "POST",
		success : function(data){
			console.log(data);

			var term = [];
			var height = [];
			var class_average = [];
			var school_average = [];
			var average = [];
			for(var i in data) {
				term.push("Term " + data[i].term);
				height.push(data[i].student);
				class_average.push(data[i].class_average);
				school_average.push(data[i].school_average);
				average.push(data[i].average);
			}

			var chartdata = {
				labels: term,
				datasets: [
					{
						label: "Student "+param,
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(59, 89, 152, 0.75)",
						borderColor: "rgba(59, 89, 152, 1)",
						pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
						pointHoverBorderColor: "rgba(59, 89, 152, 1)",
						data: height
					},
					{
						label: "Class average "+param,
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(29, 202, 255, 0.75)",
						borderColor: "rgba(29, 202, 255, 1)",
						pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
						pointHoverBorderColor: "rgba(29, 202, 255, 1)",
						data: class_average
					},
					 {
						label: "School average "+param,
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(211, 72, 54, 0.75)",
						borderColor: "rgba(211, 72, 54, 1)",
						pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
						pointHoverBorderColor: "rgba(211, 72, 54, 1)",
						data: school_average
					},

					 {
						label: "RS Average "+param,
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(49, 180, 0)",
						borderColor: "rgba(49, 180, 1)",
						pointHoverBackgroundColor: "rgba(49, 180, 1)",
						pointHoverBorderColor: "rgba(49, 180, 1)",
						data: average
					}
				]
			};

			var ctx = $("#mycanvas");

			var LineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata
			});
		},
		error : function(data) {

		}
	});
});
