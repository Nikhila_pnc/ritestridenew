<?php session_start();
include "header.php";

$page="Changepassword";
include "sidebar.php";
$stu=$db->prepare("select * from students where student_id='".$student_id."'");
$stu->execute();
$details = $stu->fetch();
$st_id=$details['st_id'];

?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo "> <a href='changepwd.php' title='Go to Home' class='tip-bottom'>".$page;?></a></div>
  </div>
<?php
if($_REQUEST['submit']=="Submit")
{
	$stid = $_REQUEST['stid'];	
	$password= $_REQUEST['password'];

	
	try{
		$stmt = $db->prepare("UPDATE students SET password =? WHERE st_id=?");
		$stmt->execute(array($password,$stid));

	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='profile.php';</script>";
}
?>


<br><br>
<div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Change Password</h3>
        </div><br>
        <div class="widget-content"  style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%;">
          <form action="changepwd.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
           
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  New Password :</label>
              <div class="controls">
                <input type="password" class="" placeholder="New Password" name="password" id="password" value="<?php echo $_REQUEST['password'];?>"  required/>
		 <input type="hidden" class="" placeholder="" name="stid" id="stid" value="<?php echo $details['st_id'];?>"  required/>			
              </div>
            </div>
	  
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Submit" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>

</div>
<script>
var divs = ["register"];
var visibleDivId = null;
function show(divId) {
  if(visibleDivId === divId) {
    //visibleDivId = null;
  } else {
    visibleDivId = divId;
  }
  hideNonVisibleDivs();
}
function hideNonVisibleDivs() {
  var i, divId, div;
  for(i = 0; i < divs.length; i++) {
    divId = divs[i];
    div = document.getElementById(divId);
    if(visibleDivId === divId) {
      div.style.display = "block";
    } else {
      div.style.display = "none";
    }
  }
}
</script>
<?php
include "footer.php";
?>

 
