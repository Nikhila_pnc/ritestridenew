<?php 
$year =$_REQUEST['year'];
$sid =$_REQUEST['id'];
$about=$_REQUEST['param'];
?>
<!DOCTYPE html>
<html>
  <head>
    <title>LineGraph :- <?php echo ucfirst($about);?> </title>
    <style>
      .chart-container {
        width: 640px;
        height: auto;
      }
    </style>
  </head>
  <body>
    <div class="chart-container">
	<input type="hidden" name="year" id="year" value="<?php echo $year;?>">
	<input type="hidden" name="id" id="id" value="<?php echo $sid;?>">
	<input type="hidden" name="param" id="param" value="<?php echo $about;?>">
      <canvas id="mycanvas"></canvas>
    </div>
    
    <!-- javascript -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/Chart.min.js"></script>
    <script type="text/javascript" src="js/linegraph.js"></script>
  </body>
</html>
