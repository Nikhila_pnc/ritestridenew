<?php
include("../includes/common.php");
//setting header to json
header('Content-Type: application/json');
$year =$_REQUEST['year'];
$sid =$_REQUEST['id'];
$param=$_REQUEST['param'];

//$param= "weight";
//$year = "2018 - 2019";
//$sid=1;
$studata=$db->prepare("select * from students where st_id = :id");
$studata->bindParam(':id',$sid);
$studata->execute();
$studatadet = $studata -> fetch();

$ft=$db->prepare("select ".$param.",term from fitness_assessment where st_id =:sid and academic_year=:year");
$ft->bindParam(':sid', $sid);
$ft->bindParam(':year', $year);
$ft->execute();
$result = $ft->fetchAll(PDO::FETCH_ASSOC);
$count = $ft->rowCount();
for($i=0;$i<$count;$i++)
{
	$ft1 = $db->prepare("select AVG(fitness_assessment.".$param.") from students,fitness_assessment where students.id_schools= :id_schools and students.class=:class and students.section= :section and fitness_assessment.academic_year=:year and students.st_id = fitness_assessment.st_id and fitness_assessment.term ='".$result[$i]['term']."'");
	$ft1->bindParam(':id_schools', $studatadet['id_schools']);
	$ft1->bindParam(':class', $studatadet['class']);
	$ft1->bindParam(':section', $studatadet['section']);
	$ft1->bindParam(':year', $year);
	$ft1->execute();
	$result1 = $ft1->fetch();

	$ft2 = $db->prepare("select AVG(fitness_assessment.".$param.") from students,fitness_assessment where students.id_schools= :id_schools and students.class=:class and fitness_assessment.academic_year=:year and students.st_id = fitness_assessment.st_id and fitness_assessment.term ='".$result[$i]['term']."'");
	$ft2->bindParam(':class', $studatadet['class']);
	$ft2->bindParam(':id_schools', $studatadet['id_schools']);
	$ft2->bindParam(':year', $year);
	$ft2->execute();
	$result2 = $ft2->fetch();

	$ft3 = $db->prepare("select AVG(fitness_assessment.".$param.") from students,fitness_assessment where students.class=:class and fitness_assessment.academic_year=:year and students.st_id = fitness_assessment.st_id and fitness_assessment.term ='".$result[$i]['term']."'");
	$ft3->bindParam(':class', $studatadet['class']);
	$ft3->bindParam(':year', $year);
	$ft3->execute();
	$result3 = $ft3->fetch();
//print_r($result3);
	$result[$i]['student'] = round($result[0][$param]);
	$result[$i]['class_average'] = round($result1[0],2);
	$result[$i]['school_average'] = round($result2[0],2);
	$result[$i]['average'] = round($result3[0],2);
}

//print_r($result);
//free memory associated with result


//now print the data
echo  json_encode($result);
