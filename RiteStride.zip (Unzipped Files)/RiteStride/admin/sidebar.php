
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li <?php if($page==""){?>class="active"<?php }?>><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li <?php if($page=="schools"){?>class="active"<?php }?>><a href="school_list.php"><i class="icon icon-building"></i> <span>Schools</span></a> </li>
    <li <?php if($page=="students"){?>class="active"<?php }?>><a href="students_list.php"><i class="icon icon-user"></i> <span>Students</span></a> </li>
    <li <?php if($page=="units"){?>class="active"<?php }?>><a href="unit_list.php"><i class="icon icon-spinner"></i> <span>Units</span></a> </li>
  </ul>
</div>
<!--sidebar-menu-->
