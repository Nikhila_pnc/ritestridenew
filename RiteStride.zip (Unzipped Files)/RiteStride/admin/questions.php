<?php session_start();
include "header.php";
include_once ('./classes/paginator.php');

$page="units";
include "sidebar.php";
$id =$_REQUEST['id'];

$sql= "select  units.*, unit_questions.* from units, unit_questions where unit_questions.id_units =units.id_units and units.id_units=".$id ;
$stmts=$db->prepare($sql);
$stmts->execute();
$num_rows = $stmts->rowCount();

if($num_rows>0){
	$pages = new Paginator;
	$pages->items_total = $num_rows;
	$pages->mid_range = 3; // Number of pages to display. Must be odd and > 3
	$pages->paginate();

	$sql=$sql." ".$pages->limit;
	$listing=$db->prepare($sql);
	$listing->execute();
	$rowct=$listing->rowCount();
	$result = $listing->fetchAll(PDO::FETCH_ASSOC);
}

?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='unit_list.php' title='Unit List' class='tip-bottom'>".$page;?></a><a href='#' title='' style="cursor:none" class='tip-bottom'>questions</a></div>
  
  </div>
<!--End-breadcrumbs-->


<!--Action boxes-->
  <div class="container-fluid"><br><br>
   <form method="post" action="school_list.php">
   <h4 align="left">Questions</h4>
   <?php  if($num_rows>0){?>
	<div align="right"><a href='unit_list.php'><input type='button' value='Units'></a> <a href='ques_add.php?id=<?php echo $id;?>'><input type='button' value='Add Questions'></a></div>
         <table cellpadding="0" cellspacing="0" align="center" style=" border: 1px solid #ccc;color:#000" width="100%" class="table">
	     <tr  style="background-color: #3973ac;color:#FFF"><td colspan="4" align="right"> <?php  echo "<span class=\"paginate\">Page: $pages->current_page of $pages->num_pages</span>";	 
			echo " &nbsp; &raquo &nbsp; ". $pages->display_pages();echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";?></td></tr> 
        
   	     <tr>
                <td align="center">&nbsp;</td>
                <td align="center"><b>Question</b></td>
                <td align="left"><b>Unit</b></td>
                <td align="center"><b>Edit</b></td>
          </tr>

<?php
$count =0;
$cr=1;
if($rowct>0){
foreach($result as $val) 
{
    if($cr%2==0)
    {$color="#CCC";}else{$color="#ECECE1";}

	   ?>
	   
          <tr bgcolor="<?php echo $color;?>" style="height:30px;">
		  <td align="center">&nbsp;</td>
          <td align="center"><?php echo $val['questions'];  ?></td>
          <td align="left"><?php echo $val['unit_name'];  ?></td>
          <td align="center"><a href="ques_edit.php?id=<?php echo $val['id_unit_ques'];  ?>"><img src="includes/icons/icon_edit.gif" width="20" height="20" border="0"></a></td>
          </tr>
                 <?php
	   $cr++;
	   $count++;
	   
	   }
   ?>		    
         <tr  style="background-color:#3973ac;color:#FFF"><td colspan="4" align="right"> <?php  echo "<span class=\"paginate\">Page: $pages->current_page of $pages->num_pages</span>";	 
			echo " &nbsp; &raquo &nbsp; ". $pages->display_pages();echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";?></td></tr> 
         </table></form> <?php  }}else echo "<h3 style='align:center'>No Content in the Database <a href='ques_add.php?id=".$id."'><input type='button' value='Add Question'></a></h3>";?>
		
<!--End-Action boxes-->    
</div>
</div>
<?php
include "footer.php";
?>

 
