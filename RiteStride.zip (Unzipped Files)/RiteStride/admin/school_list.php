<?php session_start();
include "header.php";
include_once ('./classes/paginator.php');

$page="schools";
include "sidebar.php";


$sql= "select  * from schools" ;
$stmts=$db->prepare($sql);
$stmts->execute();
$num_rows = $stmts->rowCount();

if($num_rows>0){
	$pages = new Paginator;
	$pages->items_total = $num_rows;
	$pages->mid_range = 3; // Number of pages to display. Must be odd and > 3
	$pages->paginate();

	$sql=$sql." ".$pages->limit;
	$listing=$db->prepare($sql);
	$listing->execute();
	$rowct=$listing->rowCount();
	$result = $listing->fetchAll(PDO::FETCH_ASSOC);
}

?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='school_list.php' title='Go to Home' class='tip-bottom'>".$page;?></a></div>
  
  </div>
<!--End-breadcrumbs-->


<!--Action boxes-->
  <div class="container-fluid"><br><br>
   <form method="post" action="school_list.php">
   <h4 align="left">Schools</h4>
   <?php if($num_rows>0){?>
	<div align="right"><a href='schools.php'><input type='button' value='Add School'></a></div>
         <table cellpadding="0" cellspacing="0" align="center" style=" border: 1px solid #ccc;color:#000" width="100%" class="table">
	     <tr  style="background-color: #3973ac;color:#FFF"><td colspan="4" align="right"> <?php  echo "<span class=\"paginate\">Page: $pages->current_page of $pages->num_pages</span>";	 
			echo " &nbsp; &raquo &nbsp; ". $pages->display_pages();echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";?></td></tr> 
        
   	     <tr>
                <td align="center">&nbsp;</td>
                <td align="center"><b>School Name</b></td>
                <td align="left"><b>Details</b></td>
                <td align="center"><b>Edit</b></td>
          </tr>

<?php
$count =0;
$cr=1;
if($rowct>0){
foreach($result as $val) 
{
    if($cr%2==0)
    {$color="#CCC";}else{$color="#ECECE1";}

	   ?>
	   
          <tr bgcolor="<?php echo $color;?>" style="height:30px;">
		  <td align="center">&nbsp;</td>
          <td align="center"><?php echo $val['school_name'];  ?></td>
          <td align="left"><?php echo $val['school_details'];  ?></td>
          <td align="center"><a href="school_edit.php?id=<?php echo $val['id_schools'];  ?>"><img src="includes/icons/icon_edit.gif" width="20" height="20" border="0"></a></td>
          </tr>
                 <?php
	   $cr++;
	   $count++;
	   
	   }
   ?>		    
         <tr  style="background-color:#3973ac;color:#FFF"><td colspan="4" align="right"> <?php  echo "<span class=\"paginate\">Page: $pages->current_page of $pages->num_pages</span>";	 
			echo " &nbsp; &raquo &nbsp; ". $pages->display_pages();echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";?></td></tr> 
         </table></form> <?php  }}else echo "<h3 style='align:center'>No Content in the Database <a href='schools.php'><input type='button' value='Add School'></a></h3>";?>
		
<!--End-Action boxes-->    
</div>
</div>
<?php
include "footer.php";
?>

 
