<?php session_start();
include "header.php";
include_once ('./classes/paginator.php');
$instance = new dbfunctions;
$page="Forms";
include "sidebar.php";

//list of students
$stu=$db->prepare("select * from students");
$stu->execute();
$row = $stu->fetchAll();

//list of schools
$schools=$db->prepare("select * from schools");
$schools->execute();
$rows = $schools->fetchAll();


?>

<!--main-container-part-->

<!--Action boxes-->

<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">


<div id="assess">
 <h3 style="text-align:left">Assessment</h3>
 <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb" style="border-radius:25px"> <a href="#fitness" onclick="show('fitness');"> <i class="icon-key"></i> Fitness  </a> </li>
        <li class="bg_rr" style="border-radius:25px"> <a href="#skills" onclick="show('skills');"> <i class="icon-certificate"></i> Skills</a> </li>
      </ul>
     </div>
 </div>
   </div>
<!--End-Action boxes-->   
<?php

if($_REQUEST['add']=="Add")
{
		 $res=$instance->fitness($_REQUEST); 
		if($res==""){
	        echo "<script  language='javascript'>window.location='assessment.php';</script>";}
}
if($_REQUEST['add1']=="Add")
{
		 $res1=$instance->skills($_REQUEST); 
	      if($res1==""){
	      echo "<script  language='javascript'>window.location='assessment.php';</script>";}
}

?>

<div class="container-fluid">
<!--Fitness Assessment of students-->
  <div  id="fitness"<?php if($res!="") {?>style="display:block;"<?php } else{?>style="display:none;"<?php }?>>
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h3>Fitness Assessment Form</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%;">
          <form action="assessment.php" method="post" class="form-horizontal" >
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>

	     <div class="control-group">
              <label class="control-label"  ><span style="color:red">*</span>  Academic Year :</label>
              <div class="controls">
		 <select name="year" id="year"  style="width:auto" required>
		<option value="">Academic Year </option>
		<option value="2018 - 2019">2018 - 2019</option>	
		<option value="2017 - 2018">2017 - 2018</option>			
		<option value="2016 - 2017">2016 - 2017</option>		
		</select>
              </div>
            </div>

	      <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Term :</label>
              <div class="controls">
		 <select name="term" id="term"  style="width:auto" required>
		<option value="">Academic Term </option>
		<option value="First">First</option>	
		<option value="Second">Second</option>			
		</select>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label"  ><span style="color:red">*</span>  Student ID :</label>
              <div class="controls">
		<select id="studentid" name="studentid" required>
                <option value="">Select</option>
		    <?php
		        for($i=0;$i<count($row);$i++){
		      echo '<option value="'.$row[$i]['st_id'].'">'.$row[$i]['student_id'].'</option>';}?>
               </select>
              </div>
            </div>


	    <div class="control-group">
              <label class="control-label"   ><span style="color:red">*</span>  Height : </label>
              <div class="controls">
                <input type="text" class="" placeholder="Height" name="height" id="height" value="<?php echo $_REQUEST['height'];?>"  required/>
		<select name="heightin" id="heightin"  style="width:auto" required>
		<option value="cm">cm</option>
		<option value="m">m</option>	
		<option value="inch">inch</option>			
		</select>
              </div>
            </div>

 	   <div class="control-group">
              <label class="control-label"  ><span style="color:red">*</span>  Weight : </label>
              <div class="controls">
                <input type="text" class="" placeholder="Weight" name="weight" id="weight" value="<?php echo $_REQUEST['weight'];?>"  required/>
		<select name="weightin" id="weightin"  style="width:auto" required>
		<option value="kg">kg</option>
		<option value="gm">gm</option>	
		<option value="pound">pound</option>			
		</select>
              </div>
            </div>
	  <div class="control-group">
              <label class="control-label"  ><span style="color:red">*</span>  BMI : </label>
              <div class="controls">
                <input type="text" class="" onfocus="focusfunction()" placeholder="BMI" name="bmi" id="bmi" value="<?php echo $_REQUEST['bmi'];?>"  readonly/>
              </div>
            </div>
 	    <div class="control-group">
              <label class="control-label"  ><span style="color:red">*</span>  Endurance : <br> (Sit-ups)</label>
              <div class="controls">
                <input type="text" class="" placeholder="Endurance" name="endurance" id="endurance" value="<?php echo $_REQUEST['endurance'];?>"  required/>
              </div>
            </div>
		
 	   <div class="control-group">
              <label class="control-label"   ><span style="color:red">*</span>  Upper body  : <br>(Medicine Ball Throw) </label>
              <div class="controls">
                <input type="text" class="" placeholder="Upper body explosive strength" name="es_upperbody" id="es_upperbody" value="<?php echo $_REQUEST['es_upperbody'];?>"  required/>
              </div>
            </div>

	   <div class="control-group">
              <label class="control-label"   ><span style="color:red">*</span>  Lower body : <br>(Standing Broad Jump) </label>
              <div class="controls">
                <input type="text" class="" placeholder="Lower body explosive strength" name="es_lowerbody" id="es_lowerbody" value="<?php echo $_REQUEST['es_lowerbody'];?>"  required/>
              </div>
            </div>
			
	  <div class="control-group">
              <label class="control-label"   ><span style="color:red">*</span>  Flexibility : <br> (Sit & reach) </label>
              <div class="controls">
                <input type="text" class="" placeholder="Flexibility" name="flexibility" id="flexibility" value="<?php echo $_REQUEST['flexibility'];?>"  required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label"   ><span style="color:red">*</span>  Speed Test : <br> (20 meters dash)</label>
              <div class="controls">
                <input type="text" class="" placeholder="Speed Test" name="stest" id="stest" value="<?php echo $_REQUEST['stest'];?>"  required/>
              </div>
            </div>
 	  <div class="control-group">
              <label class="control-label"   ><span style="color:red">*</span>  Grade : </label>
              <div class="controls">
                <select name="overall" id="overall"  style="width:auto" required>
		<option value="">Overall Grade</option>
		<option value="AD">AD</option>	
		<option value="EV">EV</option>			
		<option value="NO">NO</option>		
		<option value="NA">NA</option>	
		</select>
              </div>
            </div>

            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="add" value="Add" class="btn btn-success" />  <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
	<span style="color:red">*</span> Mandatory
        </div>
      </div>

<!--Fitness Assessment of students-->


<!--Skills Assessment of students-->
 <div  id="skills" <?php if($res1!="") {?>style="display:block;"<?php } else{?>style="display:none;"<?php }?>
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h3>Skill Assessment Form</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%;">
          <form action="assessment.php" method="post" class="form-horizontal" >
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>

	    <div class="control-group">
              <label class="control-label"   ><span style="color:red">*</span>  Academic Year :</label>
              <div class="controls">
		 <select name="year" id="year"  style="width:auto" required>
		<option value="">Academic Year </option>
		<option value="2018 - 2019">2018 - 2019</option>	
		<option value="2017 - 2018">2017 - 2018</option>			
		<option value="2016 - 2017">2016 - 2017</option>		
		</select>
              </div>
            </div>

	    <div class="control-group">
              <label class="control-label"   ><span style="color:red">*</span>  Student ID :</label>
              <div class="controls">
		<select id="studentid" name="studentid" required  onchange="showunit(this.value)">
                <option value="">Select</option>
		    <?php
		        for($i=0;$i<count($row);$i++){
		      echo '<option value="'.$row[$i]['st_id'].'">'.$row[$i]['student_id'].'</option>';}?>
               </select>
              </div>
            </div>
            <div id="units"></div>
	<span style="color:red">*</span> Mandatory
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="add1" value="Add" class="btn btn-success" />  <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>

	<table width="100%"><tr><td>&nbsp;&nbsp;</td><td> <b>AD</b> - Advanced </td>  <td><b> EV</b> - Evolved   </td> </tr><tr><td>&nbsp;&nbsp;</td><td>     <b>NO </b>- Novice </td><td>  <b>NA</b> - Not Applicable</td></tr></table>
        </div>
      </div>
<!--Skills Assessment of students-->

  </div>
</div>
<script>
var divs = ["assess","fitness","skills"];
var visibleDivId = null;
function show(id) {
  var i, divId, div;
  for(i = 0; i < divs.length; i++) {
    divId = divs[i];
    div = document.getElementById(divId);
    if((id==="fitness") && (id === divId))
    {	
	document.getElementById("assess").style.display ="block";
	div.style.display = "block"; 
     }
	else if((id==="skills") && (id === divId))
    {	
	document.getElementById("assess").style.display ="block";
	div.style.display = "block"; 
     }
	else{
	    if(id === divId) {
	      div.style.display = "block"; 
	    } else {
	      div.style.display = "none";
	    }
	}
  }
}

</script>

<script>
function focusfunction()
{
   var height = document.getElementById("height").value;
   var heightin = document.getElementById("heightin").value;
   var weight = document.getElementById("weight").value;
   var weightin = document.getElementById("weightin").value;
   if(heightin == "cm") {height = height/100;}
   if(weightin=="g") {weight=weight/1000;}

   if((heightin =="inch") && (weightin=="pound"))
   {
	value = 703 * (weight/(height*height));
   }
   else 
    {
		value = weight/(height*height);
                value=value.toFixed(2);
	}
	    document.getElementById("bmi").value=value;
}
</script>
<script>
function closed()
{
	window.location.href="assessment.php";
}
function showunit(str) {
  var xhttp;    
  if (str == "") {
    document.getElementById("units").innerHTML = "";
    return;
  }
  
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("units").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "getunits.php?q="+str, true);
  xhttp.send();
}
</script>
<style>
input[type=text],input[type=number],input[type=email],input[type=password],select,textarea{
max-width:100%;
}

</style>
<?php
include "footer.php";
?>

 
