<?php session_start();
include "header.php";

$page="units";
include "sidebar.php";

$unitid =$_REQUEST['id'];

	$comp=$db->prepare("select * from unit_assign where id_units = :id");
	$comp->bindParam(':id',$unitid);
	$comp->execute();
	$compdet = $comp -> fetch();
	$classes= explode(",",$compdet['classes']);

?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='unit_list.php' title='Unit List' class='tip-bottom'>".$page;?></a><a href='#' title='' style="cursor:none" class='tip-bottom'>assign</a></div>
  </div>
<?php
if($_REQUEST['submit']=="Update")
{
	 $selectedOption="";
	for ($i=0;$i<count($_REQUEST['class']);$i++)
	{
		if( $selectedOption==""){ $selectedOption = $_REQUEST['class'][$i];}
   		else $selectedOption =  $selectedOption.",".$_REQUEST['class'][$i];
	}
	 
	 $unitid = $_REQUEST['unitid'];	

	try{
		if($_REQUEST['id_assign']=="")
		{
			$stmt = $db->prepare("INSERT INTO unit_assign(classes, id_units) VALUES (:classes,:unitid)");
			$stmt->bindParam(':classes', $selectedOption);
			$stmt->bindParam(':unitid', $unitid);
			$stmt->execute();
		}
		else
		{
			$stmt = $db->prepare("UPDATE unit_assign SET classes =? WHERE id_unit_assign=?");
			$stmt->execute(array($selectedOption,$_REQUEST['id_assign']));	
		}
			
	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='unit_list.php';</script>";
}
?>


<br><br>
<div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Assign classes</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:50%;background-color:#fff;color:#000;margin-left:5%">
          <form action="assign.php" method="post" class="form-horizontal">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
         	   <div class="control-group">
              <label class="control-label">  Class :</label>
	   <div class="controls">
		<select id="class[]" name="class[]" required multiple>
                <option value="">Select</option>
		<option value="Pre-KG"  <?php if (in_array("Pre-KG", $classes)) echo "selected=selected";?>>Pre-KG</option>
		<option value="LKG"  <?php if (in_array("LKG", $classes)) echo "selected=selected";?>>LKG</option>
		<option value="UKG"  <?php if (in_array("UKG", $classes)) echo "selected=selected";?>>UKG</option>
		     <?php for($i=1;$i<=12;$i++){?>
		      <option value="<?php echo $i;?>" <?php if (in_array($i, $classes)) echo "selected=selected";?> ><?php echo $i;?></option>
			<?php }?>
               </select>
              </div>
            </div>
	                  <input type="hidden" name="unitid" value=<?php echo $unitid;?>>		
	                  <input type="hidden" name="id_assign" value=<?php echo $compdet['id_unit_assign'];?>>		
            <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Update" class="btn btn-success" /> <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>

</div>
<script>
function closed()
{
	window.location.href="unit_list.php";
}
</script>
<?php
include "footer.php";
?>

 
