<?php
 
class Paginator{
    var $items_per_page;
    var $items_total;
    var $current_page;
    var $num_pages;
    var $mid_range;
    var $low;
    var $high;
    var $limit;
    var $return;
    var $default_ipp =50;
 
    function Paginator()
    {
        $this->current_page = 1;
        $this->mid_range =5;
        $this->items_per_page = (!empty($_GET['ipp'])) ? $_GET['ipp']:$this->default_ipp;
        $this->db = new PDO('mysql:host=localhost;dbname=rite_stride;charset=utf8mb4','jivass-db','Caldam@017');
    }
 
    function paginate()
    {
        if($_GET['ipp'] == 'All')
        {
            $this->num_pages = ceil($this->items_total/$this->default_ipp);
            $this->items_per_page = $this->default_ipp;
        }
        else
        {
            if(!is_numeric($this->items_per_page) OR $this->items_per_page <= 0) $this->items_per_page = $this->default_ipp;
            $this->num_pages = ceil($this->items_total/$this->items_per_page);
        }
        $this->current_page = (int) $_GET['page']; // must be numeric > 0
        if($this->current_page < 1 Or !is_numeric($this->current_page)) $this->current_page = 1;
        if($this->current_page > $this->num_pages) $this->current_page = $this->num_pages;
        $prev_page = $this->current_page-1;
        $next_page = $this->current_page+1;
 
        $link = "";
        $linkstring = explode("?","http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]);
        if(count($linkstring)==1)  $link = $linkstring[0]."?"; 
        else{
        		
        	 	if(strpos($linkstring[1],'page')===false)
        	 	{  
        	 		$link = $linkstring[0]."?".$linkstring[1]."&";
        	 	}
        	 	else 
        	 	{
        	 		$ques=explode("page", $linkstring[1]);
        	 		$link = $linkstring[0]."?".$ques[0];
        	 	}
        	 	
      	    }
      	    $link = str_replace(" ", "", $link);
      	    
        if($this->num_pages > 1)
        {        	       	
            $this->return = ($this->current_page != 1 And $this->items_total >= 10) ? "<a class=\"paginate\" href=\"".trim($link)."page=".$prev_page."&ipp=".$this->items_per_page."\">Prev</a> ":"<span class=\"inactive\" href=\"#\">Prev</span> ";
 
            $this->start_range = $this->current_page - floor($this->mid_range/2);
            $this->end_range = $this->current_page + floor($this->mid_range/2);
 
            if($this->start_range <= 0)
            {
                $this->end_range += abs($this->start_range)+1;
                $this->start_range = 1;
            }
            if($this->end_range > $this->num_pages)
            {
                $this->start_range -= $this->end_range-$this->num_pages;
                $this->end_range = $this->num_pages;
            }
            $this->range = range($this->start_range,$this->end_range);
 
            for($i=1;$i<=$this->num_pages;$i++)
            {
                if($this->range[0] > 2 And $i == $this->range[0]) $this->return .= " ... ";
                // loop through all pages. if first, last, or in range, display
                if($i==1 Or $i==$this->num_pages Or in_array($i,$this->range))
                {
                    $this->return .= ($i == $this->current_page And $_GET['page'] != 'All') ? "<a title=\"Go to page $i of $this->num_pages\" class=\"current\" href=\"#\">$i</a> ":"<a class=\"paginate\" title=\"Go to page $i of $this->num_pages\" href='".$link."page=".$i."&ipp=".$this->items_per_page."'>$i</a> ";
                }
                if($this->range[$this->mid_range-1] < $this->num_pages-1 And $i == $this->range[$this->mid_range-1]) $this->return .= " ... ";
            }
          $this->return .= (($this->current_page != $this->num_pages And $this->items_total >= 10) And ($_GET['page'] != 'All')) ? "<a class=\"paginate\" href='".$link."page=".$next_page."&ipp=".$this->items_per_page."'>Next</a>\n":"<span class=\"inactive\" href=\"#\">Next</span>\n";
          //  $this->return .= ($_GET['page'] == 'All') ? "<a class=\"current\" style=\"margin-left:10px\" href=\"#\">All</a> \n":"<a class=\"paginate\" style=\"margin-left:10px\" href=\"$link page=1&ipp=All\">All</a> \n";
        }
        else
        {
            for($i=1;$i<=$this->num_pages;$i++)
            {
                $this->return .= ($i == $this->current_page) ? "<a class=\"current\" href=\"#\">$i</a> ":"<a class=\"paginate\" href=\"".$link."page=".$i."&ipp=".$this->items_per_page."\">".$i."</a> ";
            }
             //if($this->num_pages > 1)  $this->return .= "<a class=\"paginate\" href=\"$link page=1&ipp=All\">All</a> \n";
        }
        $this->low = ($this->current_page-1) * $this->items_per_page;
        $this->high = ($_GET['ipp'] == 'All') ? $this->items_total:($this->current_page * $this->items_per_page)-1;
        $this->limit = ($_GET['ipp'] == 'All') ? "":" LIMIT $this->low,$this->items_per_page";
    }
 
    function display_items_per_page()
    {
        $link = "";
        $linkstring = explode("?", "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]);
        if(count($linkstring)==1)  $link = $linkstring[0]."?";
        else{ 
        	 	if(strpos($linkstring[1],'page')===false)
        	 	{ $link = $linkstring[0]."?".$linkstring[1]."&";}
        	 	else 
        	 	{
        	 		$ques=explode("page", $linkstring[1]);
        	 		$link = $linkstring[0]."?".$ques[0];
        	 	}
      	    }
      	    $link = str_replace(" ", "", $link);
      	    
        $items = '';
        $ipp_array = array(10,25,50);
        foreach($ipp_array as $ipp_opt)    $items .= ($ipp_opt == $this->items_per_page) ? "<option selected value=\"$ipp_opt\">$ipp_opt</option>\n":"<option value=\"$ipp_opt\">$ipp_opt</option>\n";
        return "<span class=\"paginate\">&nbsp; &nbsp;items per page : </span><select id='selectid' class=\"paginate\" onchange=\"window.location='".$link."page=1&ipp='+this[this.selectedIndex].value;return false\">$items</select>\n";
    }
 
    function display_jump_menu()
    {
     $link = "";
        $linkstring = explode("?", "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]);
        if(count($linkstring)==1)  $link = $linkstring[0]."?";
        else{ 
        	 	if(strpos($linkstring[1],'page')===false)
        	 	{ $link = $linkstring[0]."?".$linkstring[1]."&";}
        	 	else 
        	 	{
        	 		$ques=explode("page", $linkstring[1]);
        	 		$link = $linkstring[0]."?".$ques[0];
        	 	}
      	    }
      	    $link = str_replace(" ", "", $link);
      	    
        for($i=1;$i<=$this->num_pages;$i++)
        {
            $option .= ($i==$this->current_page) ? "<option value=\"$i\" selected>$i</option>\n":"<option value=\"$i\">$i</option>\n";
        }
        return "<span class=\"paginate\">&nbsp; &nbsp;page : </span><select id='selectid' class=\"paginate\" onchange=\"window.location='".$link."page='+this[this.selectedIndex].value+'&ipp=$this->items_per_page';return false\">$option</select>\n";
    }
 
    function display_pages()
    {
        return $this->return;
    }
    
    /* adding deleted contents as a backup */
        
        function deleted_personal_details($arr,$table)
	{
                      
		$stmt = $this->db->prepare("INSERT INTO ".$table." (personal_firstname, personal_lastname, personal_email, personal_phoneno, designation, register_date, confirmed_date, approved_date, status, honorifics, company_id, deleted_date)
    VALUES (:firstname, :lastname, :email, :phoneno, :designation, :register_date, :confirmed_date, :approved_date, :status, :honorifics, :company_id, :deleted_date)");
                
		$stmt->bindParam(':firstname', $firstname);
		$stmt->bindParam(':lastname', $lastname);
		$stmt->bindParam(':email', $email);
                $stmt->bindParam(':phoneno', $phoneno);
                $stmt->bindParam(':designation', $designation);
                $stmt->bindParam(':register_date', $register_date);                
		$stmt->bindParam(':confirmed_date', $confirmed_date);
		$stmt->bindParam(':approved_date', $approved_date);
                $stmt->bindParam(':status', $status);
                $stmt->bindParam(':honorifics', $honorifics);
                $stmt->bindParam(':company_id', $company_id);
                $stmt->bindParam(':deleted_date', $deleted_date);
		
		$firstname=$arr['personal_firstname'];
		$lastname=$arr['personal_lastname'];
		$email=$arr['personal_email'];
                $phoneno=$arr['personal_phoneno'];
                $designation=$arr['designation'];
                $register_date=$arr['register_date'];
                $confirmed_date=$arr['confirmed_date'];
                $approved_date=$arr['approved_date'];
                $status=$arr['status'];
                $honorifics=$arr['honorifics'];
                $company_id=$arr['company_id'];
                $deleted_date=date('Y-m-d');
                
                $stmt->execute();
               

		$id = $this->db->lastInsertId();
		return $id;
       	}
        
          //adding company details
        function deleted_company_profile($id,$delid)
        {
            $ld=$this->db->prepare("select * from login_details WHERE personal_id =  :personalid");
            $ld->bindParam(':personalid', $delid);  
            $ld->execute();
            $ldet = $ld -> fetch();
            
            //login information 
            $stmt1 = $this->db->prepare("INSERT INTO login_details_delete (login_username, login_password, personal_id)
    VALUES (:loginid, :loginpwd, :personal_id)");
		$stmt1->bindParam(':loginid', $ldet['login_username']);
		$stmt1->bindParam(':loginpwd', $ldet['login_password']);
		$stmt1->bindParam(':personal_id', $id);               
		$stmt1->execute();
            
             //company details
                                
            $cd=$this->db->prepare("select * from company_details WHERE personal_id =  :personalid");
            $cd->bindParam(':personalid', $delid);  
            $cd->execute();
            $cdet = $cd -> fetch();
            
            $stmt2 = $this->db->prepare("INSERT INTO company_details_delete (company_id, personal_id, service_type_id, org_type_id, company_address, country_id, states_id, zip_code, company_phone, company_keypdt, commencement_year, no_of_employees, annual_sales)
    VALUES (:companyid, :personal_id, :service_type_id, :org_type_id, :company_add, :country_id, :states_id, :zipcode, :landline, :keypdt, :parentyear, :nofemployees, :sales)");
            $stmt2->bindParam(':companyid', $cdet['company_id']);
            $stmt2->bindParam(':personal_id', $id);
            $stmt2->bindParam(':service_type_id', $cdet['service_type_id']);
            $stmt2->bindParam(':org_type_id', $cdet['org_type_id']);
            $stmt2->bindParam(':company_add', $cdet['company_address']);
            $stmt2->bindParam(':country_id', $cdet['country_id']);
            $stmt2->bindParam(':states_id', $cdet['states_id']);
            $stmt2->bindParam(':zipcode', $cdet['zip_code']);
            $stmt2->bindParam(':landline', $cdet['company_phone']);     
            $stmt2->bindParam(':keypdt', $cdet['company_keypdt']);
            $stmt2->bindParam(':parentyear', $cdet['commencement_year']);
            $stmt2->bindParam(':nofemployees', $cdet['no_of_employees']); 
            $stmt2->bindParam(':sales', $cdet['annual_sales']); 
            $stmt2->execute();  
            $company_details_id = $this->db->lastInsertId();
           
            $compd=$this->db->prepare("select * from company_domains WHERE company_details_id = :company_details_id");
            $compd->bindParam(':company_details_id', $cdet['company_details_id']);  
            $compd->execute();
            $domain = $compd ->fetchAll();
            print_r($domain);
            $dcount = $compd->rowCount();
            for($i=0;$i<$dcount;$i++)
            {
                $stmt3 = $this->db->prepare("INSERT INTO company_domains_delete (domain_id, company_details_id)
        VALUES (:domain_id, :company_details_id)");
                $stmt3->bindParam(':domain_id', $domain[$i]['domain_id']);           
                $stmt3->bindParam(':company_details_id', $company_details_id);
                $stmt3->execute();
           }
            
            $compsc=$this->db->prepare("select * from company_category WHERE company_details_id = :company_details_id");
            $compsc->bindParam(':company_details_id', $cdet['company_details_id']);  
            $compsc->execute();
            $servicecat = $compsc ->fetchAll();
            $sccount = $compsc->rowCount();

            for($ii=0;$ii<$dcount;$ii++)
            {
                $stmt4 = $this->db->prepare("INSERT INTO company_category_delete (service_category_id, company_details_id)
        VALUES (:service_category_id, :company_details_id)");
                $stmt4->bindParam(':service_category_id', $servicecat[$ii]['service_category_id']);           
                $stmt4->bindParam(':company_details_id', $company_details_id);
                $stmt4->execute();
           }
           
            //company in world details         
            $compwd=$this->db->prepare("select * from company_world_details WHERE company_details_id = :company_details_id");
            $compwd->bindParam(':company_details_id', $cdet['company_details_id']);  
            $compwd->execute();
            $cwdet = $compwd ->fetch();

            $stmt5 = $this->db->prepare("INSERT INTO company_world_details_delete (company_details_id, company_world_address, company_world_phone)
        VALUES (:company_details_id, :company_world_address, :company_world_phone)");      
            $stmt5->bindParam(':company_details_id', $company_details_id);
            $stmt5->bindParam(':company_world_address', $cwdet['company_world_address']);
            $stmt5->bindParam(':company_world_phone', $cwdet['company_world_phone']);
            $stmt5->execute();
                       
            //alternate contact details
            $compacd=$this->db->prepare("select * from alternate_contact WHERE company_details_id = :company_details_id");
            $compacd->bindParam(':company_details_id', $cdet['company_details_id']);  
            $compacd->execute();
            $acdet = $compacd ->fetch();
         
            $stmt6 = $this->db->prepare("INSERT INTO alternate_contact_delete (company_details_id, contact_name_1, contact_desig_1, contact_email_1, contact_dept_1, contact_phone_1, contact_name_2, contact_desig_2, contact_email_2, contact_dept_2, contact_phone_2)
    VALUES (:company_details_id, :contact_name_1, :contact_desig_1, :contact_email_1, :contact_dept_1, :contact_phone_1, :contact_name_2, :contact_desig_2, :contact_email_2, :contact_dept_2, :contact_phone_2)");
            $stmt6->bindParam(':company_details_id', $company_details_id);          
	    $stmt6->bindParam(':contact_name_1', $acdet['contact_name_1']);
	    $stmt6->bindParam(':contact_desig_1', $acdet['contact_desig_1']);     
            $stmt6->bindParam(':contact_email_1', $acdet['contact_email_1']);          
            $stmt6->bindParam(':contact_dept_1', $acdet['contact_dept_1']);
            $stmt6->bindParam(':contact_phone_1', $acdet['contact_phone_1']); 
            $stmt6->bindParam(':contact_name_2', $acdet['contact_name_2']);
            $stmt6->bindParam(':contact_desig_2', $acdet['contact_desig_2']);     
            $stmt6->bindParam(':contact_email_2', $acdet['contact_email_2']);          
            $stmt6->bindParam(':contact_dept_2', $acdet['contact_dept_2']);
            $stmt6->bindParam(':contact_phone_2', $acdet['contact_phone_2']); 
            $stmt6->execute();
                
            //questionnaire
            $compqr=$this->db->prepare("select * from company_questionnaire WHERE company_details_id = :company_details_id");
            $compqr->bindParam(':company_details_id', $cdet['company_details_id']);  
            $compqr->execute();
            $compqrdet = $compqr ->fetch();
                     
            $stmt7 = $this->db->prepare("INSERT INTO company_questionnaire_delete (company_details_id, challenges1, challenges2, challenges3, third_party_business1, third_party_business2, third_party_business3)
    VALUES (:company_details_id, :challenges1, :challenges2, :challenges3, :third_party_business1, :third_party_business2, :third_party_business3)");
	    $stmt7->bindParam(':company_details_id', $company_details_id); 
            $stmt7->bindParam(':challenges1', $compqrdet['challenges1']);
            $stmt7->bindParam(':challenges2', $compqrdet['challenges2']);
            $stmt7->bindParam(':challenges3', $compqrdet['challenges3']);  
            $stmt7->bindParam(':third_party_business1', $compqrdet['third_party_business1']);
            $stmt7->bindParam(':third_party_business2', $compqrdet['third_party_business2']);
            $stmt7->bindParam(':third_party_business3', $compqrdet['third_party_business3']);  
            $stmt7->execute();
            
            $id = $this->db->lastInsertId();
	    return $id;
        }
   function contants_delete($id,$table)
   {
       if($table=="domains"){$tableid="domain";}
       else{$tableid=$table;}
        echo $sql = "DELETE FROM ".$table." WHERE ".$tableid."_id =  :id";
        $stmt= $this->db->prepare($sql);
	$stmt->bindParam(':id', $id);  
	$stmt->execute();
   }
   function subcontants_delete($id,$table,$count)
   {
       if($table=="service_category"){$tableid="level";}
       elseif($table=="states"){$tableid="country";}
       else{$tableid=$table;}
       for($i=0;$i<$count;$i++)
       {
            $sql = "DELETE FROM ".$table." WHERE ".$tableid."_id =  :id";
            $stmt= $this->db->prepare($sql);
            $stmt->bindParam(':id', $id);  
            $stmt->execute();
       }
   }
   //organisation updates
        function org_updates($arr)
	{
            //print_r($arr);
            $personalid=$arr['personalid'];
            $company_details_id=$arr['cdetailsid'];
            
            //adding company name
            $companyname=$arr['companyname'];
            $regnumber=$arr['regnumber'];
            $stmt=$this->db->prepare("select * from company where company_name = :companyname");
            $stmt->bindParam(':companyname', $arr['companyname']);    
            $stmt->execute();
            $details = $stmt -> fetch();       
            $count = $stmt->rowCount();
            if($count==0)
            {
                 $stmt0 = $this->db->prepare("INSERT INTO company (company_name,registration_no)
    VALUES (:companyname, :regnumber)");
		$stmt0->bindParam(':companyname', $companyname);
                $stmt0->bindParam(':regnumber', $regnumber);
		$stmt0->execute();
                $company_id = $this->db->lastInsertId();
            }
            else{ $company_id=$details['company_id'];}
            
            //company id updating in personal details table
            $sql = "UPDATE personal_details SET company_id=?  WHERE personal_id=?";
            $query = $this->db->prepare($sql);
       	    $query->execute(array($company_id,$personalid));
           
            //company details     
            if($arr['servicetype']=="new")
            { 
                //adding service type
                $stmts=$this->db->prepare("select * from service_type where service_type_name = :newstype");
                $stmts->bindParam(':newstype', $arr['newstype']);
                $stmts->execute();
                $stdetails = $stmts -> fetch();       
                $counts = $stmts->rowCount();
                if($counts==0)
                {
                 $stmt01 = $this->db->prepare("INSERT INTO service_type (service_type_name)
    VALUES (:service_type_name)");
		$stmt01->bindParam(':service_type_name', $arr['newstype']);
		$stmt01->execute();
                $service_type_id = $this->db->lastInsertId();
                }
                else{ $service_type_id=$stdetails['service_type_id'];}
             }
             else{$service_type_id=$arr['servicetype'];}
            
            if($arr['org_type']=="new")
            {
                //adding orgnaisation type
                $stmtg=$this->db->prepare("select * from org_type where org_type_name = :orgype");
                $stmtg->bindParam(':orgype', $arr['neworgtype']);
                $stmtg->execute();
                $orgdetails = $stmtg -> fetch();       
                $countg = $stmtg->rowCount();
                if($countg==0)
                {
                 $stmt11 = $this->db->prepare("INSERT INTO org_type (org_type_name)
    VALUES (:org_type_name)");
		$stmt11->bindParam(':org_type_name', $arr['neworgtype']);
		$stmt11->execute();
                $org_type_id = $this->db->lastInsertId();
                }
                else{ $org_type_id=$orgdetails['org_type_id'];}
            }
            else{$org_type_id=$arr['org_type'];}

            if($arr['country']=="new")
            {
                 //adding country
                $stmtcty=$this->db->prepare("select * from country where country_name = :country");
                $stmtcty->bindParam(':country', $arr['newcty']);
                $stmtcty->execute();
                $ctydetails = $stmtcty -> fetch();       
                $countcty = $stmtcty->rowCount();
                if($countcty==0)
                {
                 $stmt12 = $this->db->prepare("INSERT INTO country (country_name)
    VALUES (:country_name)");
		$stmt12->bindParam(':country_name', $arr['newcty']);
		$stmt12->execute();
                $country = $this->db->lastInsertId();
                }
                else{ $country=$ctydetails['country_id'];}
            }
            else{$country=$arr['country'];}
            
            if($arr['states']=="new")
            {
                 //adding states
                $stmtst=$this->db->prepare("select * from states where states_name = :states");
                $stmtst->bindParam(':states', $arr['newstate']);
                $stmtst->execute();
                $stdetails = $stmtst -> fetch();       
                $countst = $stmtst->rowCount();
                if($countst==0)
                {
                 $stmt13 = $this->db->prepare("INSERT INTO states (country_id, states_name)
    VALUES (:country_id, :states_name)");
		$stmt13->bindParam(':states_name', $arr['newstate']);
                $stmt13->bindParam(':country_id', $country);
		$stmt13->execute();
                $states = $this->db->lastInsertId();
                }
                else{ $states=$stdetails['states_id'];}               
            }
            else{$states=$arr['states'];}

            $company_add=$arr['company_add'];
            $zip_code=$arr['zipcode'];
            if($arr['landline']==""){$landline=0;}else{ $landline=$arr['landline'];}
            $keypdt=$arr['keypdt'];
            if($arr['parentyear']==""){$parentyear=0;}else{$parentyear=$arr['parentyear'];}
            $nofemployees=$arr['noofemployees'];
            $currency=$arr['currency'];
            $sales=$arr['sales']."|".$currency;
            $stmt2 = $this->db->prepare("UPDATE company_details SET company_id=?, personal_id=?, service_type_id=?, org_type_id=?, company_address=?, country_id=?, states_id=?, zip_code=?, company_phone=?, company_keypdt=?, commencement_year=?, no_of_employees=?, annual_sales=? WHERE company_details_id=?");
            $stmt2->execute(array($company_id,$personalid,$service_type_id,$org_type_id,$company_add,$country,$states,$zip_code,$landline,$keypdt,$parentyear,$nofemployees,$sales,$company_details_id));
            
            //deleting already entered domains
            $sql = "DELETE FROM company_domains WHERE company_details_id =  :CDID";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':CDID',$company_details_id);   
            $stmt->execute();
            
            $domain=$arr['domain'];  
            $newdomain=$arr['newdomain'];
            if($newdomain!="")
            {
                $stmted = $this->db->prepare("INSERT INTO domains (domain_name)
    VALUES (:domain_name)");
		$stmted->bindParam(':domain_name', $newdomain);           
		$stmted->execute();
                $newdomainid = $this->db->lastInsertId();
                array_push($domain,$newdomainid);
            }
            $ct=count($domain);
            for($i=0;$i<$ct;$i++)
            {
                if($domain[$i]!="new")
                {
                    $stmt3 = $this->db->prepare("INSERT INTO company_domains (domain_id, company_details_id)
        VALUES (:domain_id, :company_details_id)");
                    $stmt3->bindParam(':domain_id', $domain[$i]);           
                    $stmt3->bindParam(':company_details_id', $company_details_id);
                    $stmt3->execute();
                }
             }
                        
            //deleting already entered service category
            $sql = "DELETE FROM company_category WHERE company_details_id =  :NGID";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':NGID',$company_details_id);   
            $stmt->execute();
              
           $servicecat=$arr['servicecat'];
           $newcat=$arr['newcat'];
           if($newcat!="")
            {
                $stmtsc=$this->db->prepare("select * from service_category where service_category_name = :service_category_name");
                $stmtsc->bindParam(':service_category_name', $arr['newcat']);
                $stmtsc->execute();
                $scdetails = $stmtsc -> fetch();       
                $countsc = $stmtsc->rowCount();
                if($countsc==0)
                {
                    $stmteds = $this->db->prepare("INSERT INTO service_category (service_type_id, service_category_name)
        VALUES (:service_type_id, :service_category_name)");
                    $stmteds->bindParam(':service_type_id', $service_type_id);
                    $stmteds->bindParam(':service_category_name', $newcat);           
                    $stmteds->execute();
                    $newcatid = $this->db->lastInsertId();
                }
                else{$newcatid=$scdetails['service_category_id'];}
                array_push($servicecat,$newcatid);
            }
           $ctt=count($servicecat);
           for($ii=0;$ii<$ctt;$ii++)
           {
               if($servicecat[$ii]!="new")
                {
                    $stmt4 = $this->db->prepare("INSERT INTO company_category (service_category_id, company_details_id)
        VALUES (:service_category_id, :company_details_id)");
                    $stmt4->bindParam(':service_category_id', $servicecat[$ii]);           
                    $stmt4->bindParam(':company_details_id', $company_details_id);
                    $stmt4->execute();
                }
           }
                     
            //company in world details
            if($arr['worldaddress']==""){$worldaddress="";}else{$worldaddress=$arr['worldaddress'];}
            if($arr['world_landline']==""){$world_landline="";}else{$world_landline=$arr['world_landline'];}
            $stmt5 = $this->db->prepare("UPDATE company_world_details SET company_world_address=?, company_world_phone=? WHERE company_details_id=?");
            $stmt5->execute(array($worldaddress,$world_landline,$company_details_id));
		
	}
        
        //updating alternate contact details
        function alter_updates($arr)
	{
                $cdetailsid=$arr['cdetailsid'];
		$contact1=$arr['contact1'];
		$desig1=$arr['desig1'];
                $dept1=$arr['dept1'];
		$email1=$arr['email1'];
                $phone1=$arr['phone1'];
                
                if($arr['contact2']=="")$contact2="";
		else  $contact2=$arr['contact2'];
                
                if($arr['desig2']=="")$desig2="";
		else $desig2=$arr['desig2'];
                
                if($arr['dept2']=="")$dept2="";
		else $dept2=$arr['dept2'];
                
		if($arr['email2']=="")$email2="";
		else $email2=$arr['email2'];
                
                if($arr['phone2']=="")$phone2=0;
		else  $phone2=$arr['phone2'];
                
                
		$sql = "UPDATE alternate_contact SET contact_name_1=?, contact_desig_1=?, contact_email_1=?, contact_dept_1=?, contact_phone_1=?, contact_name_2=?, contact_desig_2=?, contact_email_2=?, contact_dept_2=?, contact_phone_2=?  WHERE company_details_id=?";
       		$query = $this->db->prepare($sql);
       		$query->execute(array($contact1,$desig1,$email1,$dept1,$phone1,$contact2,$desig2,$email2,$dept2,$phone2,$cdetailsid));
	
	}
        
}
?>
