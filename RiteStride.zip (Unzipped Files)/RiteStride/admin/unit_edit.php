<?php session_start();
include "header.php";

$page="units";
include "sidebar.php";

$id =$_REQUEST['id'];

	$comp=$db->prepare("select * from units where id_units = :id");
	$comp->bindParam(':id',$id);
	$comp->execute();
	$compdet = $comp -> fetch();

?>

<!--main-container-part-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><?php if($page!="") echo " <a href='unit_list.php' title='Unit List' class='tip-bottom'>".$page;?></a><a href='#' title='' style="cursor:none" class='tip-bottom'>edit</a></div>
  </div>
<?php
if($_REQUEST['submit']=="Update")
{
	$id= $_REQUEST['id'];
	$unit = $_REQUEST['unit'];	
	$objective= $_REQUEST['objective'];
	$target_dir = "img/units/";
	$target_file = $target_dir . basename($_FILES["image"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

	try{
		$stmt = $db->prepare("UPDATE units SET unit_name =?, objective=? WHERE id_units=?");
		$stmt->execute(array($unit,$objective,$id));	
		
		$temp = explode(".", $_FILES["image"]["name"]);		
		$newfilename = $id . '.' . $temp[1];

                $check = getimagesize($_FILES["image"]["tmp_name"]);
		if($check !== false) {
       			 echo "File is an image - " . $check["mime"] . ".";
       			 $uploadOk = 1;
    		} else {
			echo "File is not an image.";
			 $uploadOk = 0;
    			}	

		// Check if file already exists
		if (file_exists($newfilename)) {
    		    unlink($newfilename);
		    $uploadOk = 1;
		}
		// Check file size
		if ($_FILES["image"]["size"] > 5000000) {
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir.$newfilename)) {
			echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
			$stmt1 = $db->prepare("UPDATE units SET image =? WHERE id_units=?");
			$stmt1->execute(array($newfilename,$id));
				
		    } else {
			echo "Sorry, there was an error uploading your file.";
		    }
		}		
	   }
	catch(PDOException $e)
	   {
		echo "Connection failed: " . $e->getMessage();
	   }
	echo "<script  language='javascript'>window.location='unit_list.php';</script>";
}
?>


<br><br>
<div id="register">
	<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span> 
          <h3>Unit Details</h3>
        </div><br>
        <div class="widget-content" style="border:1px solid #CCC;width:50%;background-color:#fff;color:#000;margin-left:5%">
          <form action="unit_edit.php" method="post" class="form-horizontal" enctype="multipart/form-data">
 		<?php if($res!="") {?><p style="color:#CC3300" align="center"><?=$res;?></p><?php }?>
           
 	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Unit Name :</label>
              <div class="controls">
                <input type="text" class="" placeholder="Unit Name" name="unit" id="unit" value="<?php echo $compdet['unit_name'];?>"  required/>
              </div>

	   </div>
	   <div class="control-group">
              <label class="control-label"><span style="color:red">*</span>  Objective :</label>
	   <div class="controls">
		<textarea name="objective" id="objective" placeholder="Objective" required><?php echo $compdet['objective'];?></textarea>
              </div>
            </div>
	  
  	  <div class="control-group">
              <label class="control-label">  Image :</label>
	   <div class="controls">
		<input type="file" name="image" id="image" ><img src="img/units/<?php echo $compdet['image'];?>" width="100" height="100"><br>(Size Up to 5MB)
              </div>
            </div>
	            	  	                  <input type="hidden" name="id" value=<?php echo $id;?>>		
	  <div class="form-actions" style="background-color:#fff">
		<input type="submit" name="submit" value="Update" class="btn btn-success" />  <input type="button" name="close" value="Close" onclick="closed();" class="btn btn-success" />
            </div>
          </form>
        </div>
      </div>
</div>
</div>
<script>
function closed()
{
	window.location.href="unit_list.php";
}
</script>
<?php
include "footer.php";
?>

 
